function [S]=OFR_DurationStimulus(E,P,S)
% tic
thisInter=P.InterList(P.CurrentTrialNo);
localTrialNo=P.TrialNumbers(P.CurrentTrialNo);
%['test' thisInter localTrialNo]
FrameCon=S.TimeWindow(S.CurrentTrialFrame);
[minVal maskInd]=min(abs(FrameCon-S.MaskCons));


ThisJump=P.PixelsPerDegree*(S.SpeedDegPerSec*(1/E.Computer.ScreenHz))* E.Window.LastFrameDelta./S.StimScalingRatio;
if S.CurrentTrialFrame==1
    S.OffXback=0; S.OffYback=0;
else
    S.OffXback=mod(S.OffXback+cos(S.ThisTrialDir(thisInter,localTrialNo)  ).*ThisJump-1,S.StimX)+1;
    S.OffYback=mod(S.OffYback+sin(S.ThisTrialDir(thisInter,localTrialNo)  ).*ThisJump-1,S.StimY)+1;
end
% fprintf(1,'%i, %i\n',S.OffXback, S.OffYback);
srcRect=OffsetRect([0 0 S.StimX S.StimY],S.OffXback(1),S.OffYback(1));
Screen('BlendFunction', E.Window.Pointer, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

if S.MaskOpenLoop
    if strcmpi(E.Eyetracker,'EyeLink') && E.Computer.EyeLinkConnected% EL is setup and wanted
        %          %to get a new EyeLink Sample for mask open loop feedback
        %          if Eyelink('NewFloatSampleAvailable') > 0
        %              evt = Eyelink('NewestFloatSample');
        %              xEyePos = (evt.gx);
        %              yEyePos = (evt.gy);
        %          else
        xEyePos = E.LastEyePos([1 2]);
        yEyePos = E.LastEyePos([3 4]);
        %          end
        xEyePos(xEyePos<0) =[];
        yEyePos(yEyePos<0) =[];
        EyeX(S.CurrentTrialFrame)= double(round(mean(xEyePos)));
        EyeY(S.CurrentTrialFrame)= double(round(mean(yEyePos)));
        
    elseif strcmpi(E.Eyetracker,'Tobii') && E.Computer.TobiiConnected
        
        xEyePos = E.LastEyePos(1).*E.Window.PTBScreenWidth;
        yEyePos = E.LastEyePos(2).*E.Window.PTBScreenHeight;
        
        EyeX(S.CurrentTrialFrame)= double(nanmean(xEyePos));
        EyeY(S.CurrentTrialFrame)= double(nanmean(yEyePos));
    elseif strcmpi(E.Eyetracker,'SimEyelink')
        %         [EyeX(S.CurrentTrialFrame), EyeY(S.CurrentTrialFrame)] = GetMouse(E.Settings.ScreenUsed);
        EyeX(S.CurrentTrialFrame)= E.Window.PTBScreenWidthCenter;
        EyeY(S.CurrentTrialFrame)= E.Window.PTBScreenHeightCenter;
    end
end


if S.MaskOpenLoop  && EyeX(S.CurrentTrialFrame) >= 0 && EyeY(S.CurrentTrialFrame) >= 0
    maskX = EyeX(S.CurrentTrialFrame);
    maskY = EyeY(S.CurrentTrialFrame);
else
    maskX = E.Window.PTBScreenWidthCenter;
    maskY = E.Window.PTBScreenHeightCenter;
end
S.MaskDestRect = CenterRectOnPoint([0 0 E.Window.PTBScreenWidth*S.MaskSizeMltpl E.Window.PTBScreenHeight*S.MaskSizeMltpl],maskX,maskY);


Screen('DrawTexture', E.Window.Pointer,  S.StimTex(P.ContrastInds(P.CurrentTrialNo))  , srcRect, S.DestRect); % draw stimulus
Screen('DrawTexture', E.Window.Pointer,  S.masktex(P.MaskInds(P.CurrentTrialNo),S.WhichMaskSamp(P.CurrentTrialNo),maskInd ), [], S.MaskDestRect,[],[]);   % S.DestRect % draw the mask over stimulus

if strcmpi(E.Eyetracker,'Tobii') && E.Computer.TobiiConnected
    if S.CurrentTrialFrame>1
        Screen('TextSize',E.Window.Pointer, 10);
        DrawFormattedText(E.Window.Pointer, 'HP', 50, E.Window.PTBScreenRect(4)-50, E.HeadPositionFlagCol);%
    else
        Screen('TextSize',E.Window.Pointer, 10);
        DrawFormattedText(E.Window.Pointer, 'HP', 50, E.Window.PTBScreenRect(4)-50, [0.5 0.5 0.5]);%E.Window.PTBScreenRect(4)-50
    end
end

if E.FixationOnOff  %E.Settings.IsFixationMarker(P.FixMrkInds(P.CurrentTrialNo))
    bvlDrawCrosshair(E.Window.Pointer, E.Window.PTBScreenWidth/2, E.Window.PTBScreenHeight/2 ,15 , [192 192 192] ,2);
   % Screen('TextSize',E.Window.Pointer, 14);
    %DrawFormattedText(E.Window.Pointer, '*', 'center', 'center', [0 0 0]);%E.Window.PTBScreenRect(4)-50
end

PsychColorCorrection('SetEncodingGamma', E.Window.Pointer, E.Window.GamVal );
