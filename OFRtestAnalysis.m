clear all
load('Demo_20180323162609.mat')
S=S{1}; E=E{1}; P=P{1};
WrongRight={'-' '+'};
cnt=1;
figure(1)
Hz=E.Window.ScreenHz
RunLength = P.InterTrialNo(1);
NoConds=length(P.InterTrialNo);
Dirs=[0 180];
for i=1:sum(P.InterTrialNo)
    intNo = P.InterList(i);
    triNo=  P.TrialNumbers(i);%IntTriCnt(intNo);      
    stim(intNo,triNo)=floor(P.StimulusDirection(S.StimNumber(P.InterList(i),P.TrialNumbers(i))));
    test(intNo,triNo)=P.TestValue(i);
    correct(intNo,triNo)=P.Response(i);
    keyVal(intNo,triNo)=P.ResponseKeyName{i};
    spNo=(intNo-1)*RunLength+triNo;
    subplot(3,3,i);%NoConds,RunLength,spNo)
    eTime=squeeze(E.EyePos.Time(intNo,triNo,:))';
    eData=squeeze(E.EyePos.X(intNo,triNo,:,1))';
    eData=eData(eData>0);
    eTime=eTime(eData>0);
    eData=eData-nanmean(eData);
    eTime2=eTime;
    for j=2:(length(eTime)-1)
        if isnan(eTime2(j))
            eTime2(j)=(eTime2(j-1)+eTime2(j+1))/2;
        end
    end
    eTime=eTime2(1:end-1);
    eTime=(eTime-eTime(1))./1000;
    
   diffData=1920.*(eData(1:end-1));


    
    
    MovieSdMs(intNo,triNo) = MaxMin(10^P.TestValue(i),1,500);
    MoveSdFrames=  floor(MovieSdMs(intNo,triNo)/(1000*E.Window.InterFrameInterval));
    TotalMoveLengthFrames=  floor(8*MoveSdFrames);
    TotalMoveLengthFrames=MaxMin(TotalMoveLengthFrames,1*E.Window.ScreenHz,5*E.Window.ScreenHz);
    TimeWindow=(0)+1.*GenerateGaussian(1, TotalMoveLengthFrames, MoveSdFrames,MoveSdFrames, 0,0,0);
  
    
    
    tMs=[0:TotalMoveLengthFrames-1].*(1000/Hz);
       TimeWindow2=interp1(tMs,TimeWindow,eTime);
 diffData1=TimeWindow2.*diff(1920.*eData);
   smallMoves=abs(diffData1);
    MoveDir=sign(sum(diffData(:)));
    predDir=Dirs(1+(MoveDir>0));
    plot(eTime,diffData,'b-',eTime,30.*TimeWindow2,'r-'); drawnow
    title(sprintf('%s %0.1f %d%% %d deg %d deg',WrongRight{1+correct(intNo,triNo)},S.BaseMaskLevels(P.MaskInds(i)),floor(100*S.BaseContrastLevels(P.ContrastInds(i))),stim(intNo,triNo),predDir))
   % axis([0 200 0.4 0.6]);
    fprintf(1,'[%d %d] dir %d resp %s %s \n',intNo,triNo,stim(intNo,triNo),keyVal(intNo,triNo),WrongRight{1+correct(intNo,triNo)});%stimVal0(j),stimVal(j),keyVal(j),WrongRight{respVal(j)+1});
%    IntTriCnt(intNo)=IntTriCnt(intNo)+1;
end
figure(2)
for i=1:4
     subplot(2,2,i)
semilogy(1:8,test(i,:),'o-')
end
%    
%     for j=1:length(MyTrials)
%             
%             %    E.EyePos
%             stimVal0(j)=(S.StimNumber(intLoop,j)-1)*pi;
%             respVal(j)=P.Response(MyTrials(j));
%             testVal(j)=P.TestValue(MyTrials(j));
%             keyVal(j)=P.ResponseKeyName{MyTrials(j)};
%             if (respVal(j)&keyVal(j)=='4')|(~respVal(j)&keyVal(j)=='6')
%                 stimVal(j)=0;
%             else
%                 stimVal(j)=pi;
%             end
%             fprintf(1,'[%d %d] dir %3.2f  %3.2f %s  %s \n',intLoop,j,stimVal0(j),stimVal(j),keyVal(j),WrongRight{respVal(j)+1});
%         end
%         subplot(2,2,intLoop)
%         semilogy(1:length(MyTrials),testVal,'o-')
%     end
%end
