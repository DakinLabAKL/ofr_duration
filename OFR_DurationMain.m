% OFR_Duration
OFR_DurationInit;     % load the initial settings and textures
% *************  Main experiment loop **************
NewTrial=1;
P.OverallTrialCntr = 0;
    KbReleaseWait;
    while ~KbCheck
        TobiiSample = E.Settings.Tobii.ET.get_gaze_data('flat');
         if ~isempty(TobiiSample.left_gaze_point_on_display_area)
         x1=TobiiSample.left_gaze_point_in_user_coordinate_system(1);
        y1=TobiiSample.left_gaze_point_in_user_coordinate_system(2);
        z1=TobiiSample.left_gaze_point_in_user_coordinate_system(3);
       x2=TobiiSample.left_gaze_origin_in_user_coordinate_system(1);
        y2=TobiiSample.left_gaze_origin_in_user_coordinate_system(2);
        z2=TobiiSample.left_gaze_origin_in_user_coordinate_system(3);
        dist=sqrt((x1-x2).^2+(y1-y2).^2+(z1-z2).^2);
          DrawFormattedText(E.Window.Pointer, sprintf('Sit at %2.1fcm (you are at %d cm)\nHit a key to start',round(E.Settings.TestDistCm),round(dist/10)), 'center','center', E.Window.Gray-E.Window.Gray*0.25);
        end
       Screen('Flip' , E.Window.Pointer);
    end
    KbReleaseWait;

while (P.CurrentTrialNo <= P.TotalTrials) && (~E.ExitKeyPressed) % Start Experiment.
    P.OverallTrialCntr = P.OverallTrialCntr+1; % count all the number of new trials and or repetitions
    if NewTrial
        if P.CurrentTrialNo == P.TotalTrials        % this condition means do not update after one occurance of final trial
            break
        end
        P                   = GetTestValue(P);
        trialNum            = P.CurrentTrialNo;
    end
    localTrialNo=P.TrialNumbers(P.CurrentTrialNo);
    thisInter=P.InterList(localTrialNo);
    if strcmp(E.Eyetracker, 'EyeLink')    %send feedback to Eylink screen title box
        Eyelink('command', 'record_status_message "Set%d TRIAL %d/%d  OA%d M%d/%d C%d/%d"',...
            E.Settings.ExperimentSet, P.CurrentTrialNo, P.TotalTrials, P.OverallTrialCntr, P.MaskInds(P.CurrentTrialNo),E.Settings.MaskNo ,P.ContrastInds(P.CurrentTrialNo),length(E.Settings.BaseContrastLevels));
    end
    fprintf('T%d [I%d T%d]\t Stim %+2.2f\n',trialNum,P.InterList(trialNum),P.TrialNumbers(trialNum),P.TestValue(trialNum));
    KeyPressedDuringMovie=0;
    MovieSdMs(thisInter,localTrialNo) = MaxMin(10^P.TestValue(P.CurrentTrialNo),1,500);
    MoveSdFrames=  round(MovieSdMs(thisInter,localTrialNo)/(1000*E.Window.InterFrameInterval));
    MoveSdFrames=MaxMin(MoveSdFrames,1,100000);
    TotalMoveLengthFrames=  floor(8*MoveSdFrames);
    TotalMoveLengthFrames=MaxMin(TotalMoveLengthFrames,1*E.Window.ScreenHz,5*E.Window.ScreenHz);
    E.StimFrames(thisInter,localTrialNo)=TotalMoveLengthFrames;
    S.TimeWindow=GenerateGaussian(1, TotalMoveLengthFrames, MoveSdFrames,MoveSdFrames, 0,0,0);
    
    for frameLoop = 1:E.StimFrames(thisInter,localTrialNo) % shows stimulus
        S.CurrentTrialFrame = frameLoop;
        %%%%%%%%%%%%%%% draw stimulus %%%%%%%%%%%%%
        S = OFR_DurationStimulus(E,P,S); % this writes the stimulus texture into the back buffer
        %%%%%%%%%%%%%%% gert reponse %%%%%%%%%%%%%
        E = OFR_DurationSimpleTimingGetBehaviour(E,P); % flips to display back buffer with no while in ET loop
        if ~isnan(E.LastKeyPressedOnFrame)
            KeyPressedDuringMovie = 1;
            LastKeyStored = E.LastKeyPressed;
        end
    end
    OFR_DurationSignalQualityControl;
    Screen('FillRect', E.Window.Pointer, E.Window.PTBbackground, E.Window.PTBScreenRect);
    Screen('TextSize',E.Window.Pointer, 14);
       %     DrawFormattedText(E.Window.Pointer, '*', 'center', 'center', [0 0 0]);%E.Window.PTBScreenRect(4)-50
     bvlDrawCrosshair(E.Window.Pointer, E.Window.PTBScreenWidth/2, E.Window.PTBScreenHeight/2 ,15 , [192 192 192] ,2);

    % show feedback on experiment screen
    QCstring = sprintf('Trial %i / %i \nQ: %2.2f',P.CurrentTrialNo, P.TotalTrials,E.TrialGoodDataRatio);
    Screen('TextSize',E.Window.Pointer, 10);
    DrawFormattedText(E.Window.Pointer, QCstring, 50, E.Window.PTBScreenRect(4)-50, E.Window.Gray-E.Window.Gray*0.25);
    Screen('Flip' , E.Window.Pointer);
    
    KbReleaseWait;
    GoodKey = 0; E.KeyIsDown=0;
    while ~GoodKey
        E = PsychSandboxGetKeyboardAndMouse(E);
        if  KeyPressedDuringMovie
            E.LastKeyPressed = LastKeyStored;
            E.KeyIsDown=1;
        end
        KeyMatches = (strncmpi(P.Responses,E.LastKeyPressed,6));
        E.AnyRespKeyPressed = sum(KeyMatches);
        E.RepeatKeyPressed = (strncmpi(P.RepeatKey,E.LastKeyPressed,6));
        E.ExitKeyPressed = strcmp(E.LastKeyPressed,'ESCAPE');
        GoodKey=E.RepeatKeyPressed|E.AnyRespKeyPressed|E.ExitKeyPressed;
        if KeyPressedDuringMovie && ~GoodKey % they hit an invalid key during the movie
            KeyPressedDuringMovie=0;
        end
    end
    KbReleaseWait;
    P.ResponseKeyName{P.CurrentTrialNo} = E.LastKeyPressed;
    %             P.ResponseKeyNo(P.CurrentTrialNo)   = find(KeyMatches);
    %             P.Response(P.CurrentTrialNo)        = (P.ResponseKeyNo(P.CurrentTrialNo) == P.InterList(P.CurrentTrialNo))  ;%rand>0.5;%P.SimResponse(trialNum); % Save (Simulated) Response
    CorrectKey=P.Responses(S.StimNumber(P.InterList(P.CurrentTrialNo),P.TrialNumbers(P.CurrentTrialNo)));
    
    P.Response(P.CurrentTrialNo)        = (strncmpi(CorrectKey,E.LastKeyPressed,6)) ; % new ker response scoring method
    if S.TextVisualFeedBack
        if E.RepeatKeyPressed
            DrawFormattedText(E.Window.Pointer, 'Repeat', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
        elseif E.ExitKeyPressed % exiting
            DrawFormattedText(E.Window.Pointer, 'Exiting', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
        elseif P.Response(P.CurrentTrialNo)==1 %correct?
            DrawFormattedText(E.Window.Pointer, 'Correct', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
        else %incorrect (default)
            DrawFormattedText(E.Window.Pointer, 'Incorrect', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
        end
        Screen('Flip' , E.Window.Pointer);
        WaitSecs(0.25);% define length
    end
    if S.VisualFeedback
        if E.RepeatKeyPressed
            Screen('FrameRect', E.Window.Pointer, E.Window.PTBbackground+10, E.Window.PTBScreenRect,25);
        elseif E.ExitKeyPressed % exiting
            Screen('FrameRect', E.Window.Pointer, E.Window.White, E.Window.PTBScreenRect,25);
        elseif P.Response(P.CurrentTrialNo)==1 %correct?
            Screen('FrameRect', E.Window.Pointer, [0 64 0], E.Window.PTBScreenRect,25);
        else %incorrect (default)
            Screen('FrameRect', E.Window.Pointer, [64 0 0], E.Window.PTBScreenRect,25);
        end
        Screen('Flip' , E.Window.Pointer);
        WaitSecs(0.25);% define length
    end
    if E.Settings.FeedbackLevel > 1            % feedback goes here
        fprintf('%d\t Int %d\t Trial %d\t %+2.2f dir is %d resp is %d\n',...
            trialNum,P.InterList(trialNum),P.TrialNumbers(trialNum),P.TestValue(trialNum),S.ThisTrialDir(thisInter,localTrialNo) ,((P.Response(trialNum))));
    end
    if E.RepeatKeyPressed ||( ~E.TrialGoodDataFlag )%  || (FirstTrialRpt)
        NewTrial      = 0;
        E = OFR_DurationResetRecordedValues(E,P); % reset the recorded values for the current trial (the one we want to repeat)
      else
        NewTrial=1;
        [P] = UpdateTestStructure(P); % removed ",E" from input arguments because it is not needed there
    end
     KbReleaseWait;
end
%%
% finish up
close all;
PsychSandboxShutdown;          % Save / Shutdown
%%
P1=P{1};
E1=E{1};
S1=S{1};
%%
for i=1:2
    for j=1:2
        TheInter=(i-1)*2+j
        WhichMask=S1.BaseMaskLevels(MaskNos(TheInter));
        WhichContrast=100.*S1.BaseContrastLevels(ContrastNos(TheInter));
        t(TheInter)=QuestMean(P1.q(TheInter));		% Recommended by Pelli (1989) and King-Smith et al. (1994). Still our favorite.
        sd(TheInter)=QuestSd(P1.q(TheInter));
        subplot(2,2,TheInter)
        trials=1:P1.NoFixedTrialsPerInter(TheInter);
        data=10.^(P1.q(TheInter).intensity(1:P1.NoFixedTrialsPerInter(TheInter)));
        semilogy(trials,data,'o-')
        title(sprintf('Size: %3.3f deg Contrast:%d%% Thresh: %3.1f ms',WhichMask,round(WhichContrast),10.^t(TheInter)))
    end
end

%%
%OFR_Duration_ThisRun_TBT_Analysis