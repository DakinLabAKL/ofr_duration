
% OFR_DurationProcessing

if ~isfield(S,'NewSamplingSepMS')
    S.NewSamplingSepMS      = 0; %  1 for one mili second.
end
%maxJumpPixelsPerMs        = SpeedPixelsPerDegree*MaxSaccSpeedDegPerSec/(1000*S.NewSamplingSepMS);
if S.NewSamplingSepMS
    maxJumpPixelsPerSample        = S.PixelsPerDegree*MaxSaccSpeedDegPerSec/(1000*S.NewSamplingSepMS); % expressed in  pixels per sample
    StimSpeedPixPerSample = S.PixelsPerDegree*S.SpeedDegPerSec/(1000*S.NewSamplingSepMS);
else
    maxJumpPixelsPerSample        = S.PixelsPerDegree*MaxSaccSpeedDegPerSec/(1/E.Window.InterFrameInterval); % now  pixels per frame & pixels per sample are the same thing
    StimSpeedPixPerSample =  S.PixelsPerDegree*S.SpeedDegPerSec/(1/E.Window.InterFrameInterval);
end

goodVals    = find(tDataIn>0); % used to clip end of each stream of data
xData0      = xDataIn(goodVals); yData0=yDataIn(goodVals); paData0=paDataIn(goodVals); tData0=tDataIn(goodVals);
tBase       = tData0-tData0(1);% in milisecs

FrameFlipData = FrameFlipDataIn(FrameFlipDataIn>0)*1000; % convert to mili secs
FrameFlipData= FrameFlipData- FrameFlipData(1);
% figure,plot(tBase,tBase,'b.',tBaseS1,tBaseS1,'ro',tBaseS1, FrameFlipData,'m*')

% figure,plot(1./diff(tBaseS1/1000),'bo')

x00     = xData0 - E.Window.PTBScreenWidthCenter; % change the center of rotation to the center of data
y00     = yData0 - E.Window.PTBScreenHeightCenter;  %  E.Window.PTBScreenWidthCenter

% rotate the signal. Note the input angle should be negative here
xData00 = x00.*cos(-StimulusDirection) - y00.*sin(-StimulusDirection);
yData00 = x00.*sin(-StimulusDirection) + y00.*cos(-StimulusDirection);

xData00 = xData00 + E.Window.PTBScreenWidthCenter; % change back the center to the original coordinate center %   E.Window.PTBScreenWidth;
yData00 = yData00 + E.Window.PTBScreenHeightCenter;%   E.Window.PTBScreenHeight;

AllSxData = xData00; AllSyData = yData00; AllSpaData = paData0;  AllStData = tBase; % All samples data   



%  tBasex = 1:length(tBase);
%  figure,plot(tBasex,tBase,'b.',tBasex(FSInds),tBase(FSInds),'ro',tBasex(71),tBase(71),'kd');
%  % evaluate second FSinds and plot as following
%  hold on, plot(tBasex(FSInds),tBase(FSInds),'kx');
%  figure,plot(1:length(tBaseS1),tBaseS1,'b.',1:length(FrameFlipData), FrameFlipData,'mo','MarkerSize',10)
%  figure,plot(1./diff(tData./1000))
    firstSeen = (E.Settings.CoveredFrames+1); % the first frame after covered frame

if JustFirstSample
    if isfield(E.EyePos,'OverSample') % i.e. we have first samples indices
        EachFrameSamples = (squeeze(E.EyePos.OverSample(ThisTrialIntLe, ThisIntLeRept, :, :)))'; % keep the frame flip time for later processing
        NoSampsPerFrame = sum(EachFrameSamples>0);
        EachFrameSamplesGT0 = (EachFrameSamples(EachFrameSamples>0))'; % length(EachFrameSamples)== length(xData0)? : yes
        FSInds  = find(EachFrameSamplesGT0==1); % first sample indices
       % tBaseS1 = tBase(FSInds);
        
    else %i.e we stimate first samples indices
        tDataDiff = diff([ 0 AllStData' 0]);
        FSInds    = [1, find(tDataDiff>E.Settings.MaxSamplesPerFrame.*2)]; % FS: first Sample.  *2 due te 500Hz sampling rate
       % tBaseS1 = tBase(FSInds);
end
    SampMargin = 5; % the margin before and after blinks, means 5 frames.
%     firstSeen = (E.Settings.CoveredFrames+1); % the first frame after covered frame
    tDataFS   = AllStData(FSInds);
    xDataFS   = AllSxData(FSInds);
    yDataFS   = AllSyData(FSInds);
    paDataFS  = AllSpaData(FSInds);
else % i.e we want Full samples
    SampMargin = 15; % the margin before and after blinks, means 5 frames.
%     firstSeen  = (find(tBase==tBaseS1(E.Settings.CoveredFrames+1))); % the first frame after covered frame
    tDataFS   = AllStData;
    xDataFS   = AllSxData;
    yDataFS   = AllSyData;
    paDataFS  = AllSpaData;
end

xData     = xDataFS(firstSeen:end); yData=yDataFS(firstSeen:end); paData=paDataFS(firstSeen:end); tData=tDataFS(firstSeen:end);
%
paData=paData(paData>0);
xData=xData(paData>0);
yData=yData(paData>0);
tData=tData(paData>0);


paMean      = mean(paData(paData>0));
paStd       = std(paData(paData>0));
paThresh    = 100;%paMean-paStdThresh*paStd; % SMD have changes to see the effect on Good regions, because current method will cause some data loss even in good signals
goodVals    = ((xData>=0)&(yData>=0)&(paData>paThresh)); % was the eye open? LH - changed x to y incase an error - check with steven;SMD Add >= to include zero inside x- or y- data

% plot(tData,paData,'b-',tData,tData.*0+paThresh,'g--')
% axis([0 max(tData) 0 1.1*max(paData)])

%
% OutlierThresh=3;
% AbsMADdev   = abs(paData-median(paData))./median(abs(paData-median(paData)));
% goodVals    = ((xData>=0)&(yData>=0)&(AbsMADdev<OutlierThresh));%(paData>paThresh)); % was the eye open? LH - changed x to y incase an error - check with steven;SMD Add >= to include zero inside x- or y- data


% paData=paData(goodVals);
% xData=xData(goodVals);
% yData=yData(goodVals);
% tData=tData(goodVals);

% figure(6), plot(tData,xData,'r-o')
 

SecB        = (find(diff(goodVals)>0)+SampMargin)'; % LH reworked so I understand it and can concatinate
beginInd    = [1 SecB];
SecE        = (find(diff(goodVals)<0)-SampMargin)'; % LH reworked so I understand it and can concatinate
endInd      = [SecE length(goodVals)];
% deal with blink cases in which the end of the blink is after the end of the trial
if length(beginInd)~=length(endInd) %this means that there was a blink overlapping with the start or the end of the trial
    if length(beginInd)>length(endInd)
        beginInd=beginInd(2:end); %remove first start, as it blink overlaps with beginning of trial
    else
        endInd=endInd(1:end-1); %remove last end as it blink overlaps with end of trial
    end
end
NumGoodSects = length(beginInd); %now count numbers
clear newX newY newP newTD newT
xDall = []; yDall = []; Xall = []; Yall = []; Tall = [];  tDall = [];  Pall = [];



for sect=1:NumGoodSects
    if beginInd(sect)<endInd(sect)
        %                     [beginInd(sect) endInd(sect)];  %deal with overlaping disruptions
        x0      = (xData(beginInd(sect):endInd(sect)));
        y0      = (yData(beginInd(sect):endInd(sect)));
        t0      = tData(beginInd(sect):endInd(sect));
        pa0     = paData(beginInd(sect):endInd(sect));
        if S.NewSamplingSepMS 
         newT    = (0:S.NewSamplingSepMS:t0(end));
            newX    = interp1(t0,x0,newT,'linear'); % evenly sampled in time  'pchip'  'v5cubic' 'nearest' 'spline' : v5cubic produces smoother
            newY    = interp1(t0,y0,newT,'linear'); % evenly sampled in time  'pchip'  'v5cubic'
            newP    = interp1(t0,pa0,newT,'linear'); % evenly sampled in time  'pchip'  'v5cubic'
        else
            newT = t0';
            newX = x0';
            newY = y0';
            newP = pa0';
            
        end
        notNans = (~isnan(newX))&(~isnan(newY));
        newX    = newX(notNans);
        newY    = newY(notNans);
        newT    = newT(notNans);
        
        tD1     = newT(2:end);
        newP    = newP(notNans);
        Xall = [Xall(:)' newX];  % the newX s from all GoodSectors
        Yall = [Yall(:)' newY];
        Tall = [Tall(:)' newT];
        Pall = [Pall(:)' newP];
        
        %                                 figure(90),clf,plot(t0,pa0,'gx',t0,x0,'ro',newT,newX,'r.-',t0,y0,'bo',newT,newY,'b.-');
        %                                 legend('pupil area','X position', 'newX position','Y position','newY position')
        %                                 drawnow,WaitSecs(0.5);
%         if 1 % Smooth4SpeedCalc
%             newX = sgolayfilt(double(newX),1,Sgolength);
%             newY = sgolayfilt(double(newY),1,Sgolength);
%         end
        xD1      = diff(newX);
        yD1      = diff(newY);
        
        goodVals = ((abs(xD1)<maxJumpPixelsPerSample)&(abs(yD1)<maxJumpPixelsPerSample)); %diff between 2 data points is less than max jump
        xD1      = xD1(goodVals);
        yD1      = yD1(goodVals);
        tD1      = tD1(goodVals);
        xDall    = [xDall(:)' xD1];
        yDall    = [yDall(:)' yD1];
        tDall    = [tDall(:)' tD1];
        
    end
end

%
yDall=0.*yDall;
Speed      = sqrt(xDall.^2 + yDall.^2);
SpeedAng   = atan2(yDall, xDall);
SignedSpeed = Speed.*sign(tan(SpeedAng));
Acceleration = diff(Speed);
% polarplot(log(SpeedAng),log(Speed),'ro')

clear DisRec AngRec
if ~isempty(Speed)
    maxSpeed   = max(Speed)*2; % default is multiplied by 2
    minSpeed   = StimSpeedPixPerSample; % 1.2/2; %maxSpeed./(2.^12);
    possthresh = 2.^(linspace(log2((minSpeed)),log2((maxSpeed)),64 ));  %2.^linspace(2,7,32)2.^(linspace(log2((minSpeed)),log2((maxSpeed)),32   )); % in d
else
    possthresh = 2.^linspace(log2(1.2),log2(50),64);
end
% Sac2PursRatio = 10;
% possthresh      = linspace((S.SpeedPixPerMS/Sac2PursRatio),(S.SpeedPixPerMS*Sac2PursRatio),32);
for tLoop         = 1:length(possthresh)
    threshVal     = possthresh(tLoop);
    AboveVals     = find(Speed>=threshVal); %  Speed
    BelowVals     = find(Speed<threshVal); %
    VerComponent  = sum([sin(SpeedAng(BelowVals)).*Speed(BelowVals) -sin(SpeedAng(AboveVals)).*Speed(AboveVals)]);
    HorComponent  = sum([cos(SpeedAng(BelowVals)).*Speed(BelowVals) -cos(SpeedAng(AboveVals)).*Speed(AboveVals)]);
    AngRec(tLoop) = atan2(VerComponent,HorComponent);
    DisRec(tLoop) = sqrt(VerComponent.^2+HorComponent.^2); % it is the vector of magnitude of pursuit eye speed, calculated for different saccade threshold
end
%  figure(3), semilogx((possthresh),DisRec,'o-')

[~, maxInd]     = max(DisRec);
%
% hreshold       =   S.SpeedPixPerMS.*10;%2;
Threshold       =   possthresh(maxInd); % saccade threshold


%
subplot(2,4,4)
TrackSpeedEsts=Speed;%(Speed<Threshold);
TrackSpeedEsts=TrackSpeedEsts(TrackSpeedEsts>0);
NoBins=16;
[SpFreq SpBins]=hist(log(TrackSpeedEsts),NoBins);
SpBins=double(SpBins);
SpFreq=double(SpFreq);
try
[uEst,varEst,scaleEst] = FitGaussian((SpBins),SpFreq./sum(SpFreq));
fineX=exp(linspace(log(min(TrackSpeedEsts)),log(max(TrackSpeedEsts)),1000));
pred=scaleEst.*NormalPDF(log(fineX),uEst,varEst^2);
SpeedFromHistPixPerSamp = exp(uEst);
SpeedFromHistPixPerSec = SpeedFromHistPixPerSamp*(1/E.Window.InterFrameInterval);
SpeedFromHistDegPerSec = SpeedFromHistPixPerSec/S.PixelsPerDegree;
HistGain = SpeedFromHistDegPerSec./ S.SpeedDegPerSec;
catch
    HistGain = nan;
    fprintf('The Gaussian fit to speed data is not possible; TrialNo: %i \n', P.CurrentTrialNo)
end
plot((SpBins),SpFreq./sum(SpFreq),'o',log(fineX),pred,'r-')
title(sprintf('Estimated tracking speed is %3.3f deg/s\n',SpeedFromHistDegPerSec))
%%
NoBins=16;

subplot(2,4,8)

[pAFreq pABins]=hist((Pall),NoBins);
pABins=double(pABins);
pAFreq=double(pAFreq);
[uEst,varEst,scaleEst] = FitGaussian((pABins),pAFreq./sum(pAFreq));
fineX=(linspace((min(Pall)),(max(Pall)),1000));
pred=scaleEst.*NormalPDF((fineX),uEst,varEst^2);
PupilAreaFromHistPix0 = (uEst);
plot((pABins),pAFreq./sum(pAFreq),'o',(fineX),pred,'r-')
PupilAreaFromHistPix = mean(Pall);

% Baseline Pupil size
if 0 %isfield(P, 'BaseTrialNo') % If we have baseline pupil measurement
for i = 1: P.BaseTrialNo
BaselinePupilTimeInMS = E.BaseEyePos.Time(i,:);
BaselinePupilArea = E.BaseEyePos.PA(i,:,2);
goodVals = find(BaselinePupilTimeInMS>0 & BaselinePupilArea>0);
PupilTimeInMS=BaselinePupilTimeInMS(goodVals);
PupilArea = BaselinePupilArea(goodVals);
% figure(10+i), plot(PupilTimeInMS,PupilArea,'ro' )

[pAFreq, pABins]=hist((PupilArea),NoBins);
pABins=double(pABins);
pAFreq=double(pAFreq);
[uEst,varEst,scaleEst] = FitGaussian((pABins),pAFreq./sum(pAFreq));
fineX=(linspace((min(PupilArea)),(max(PupilArea)),1000));
pred=scaleEst.*NormalPDF((fineX),uEst,varEst^2);
BasePupilAreaFromHistPix0(i) = (uEst);
% plot((pABins),pAFreq./sum(pAFreq),'o',(fineX),pred,'r-')
BasePupilArea(i) = mean(PupilArea);
end
BasePupilAreaFromHist = mean(BasePupilAreaFromHistPix0);
BasePupilAreaFromHisMax  = max(BasePupilAreaFromHistPix0);
end
%%
PredictedAng     = AngRec(maxInd); % an angle in radians
LookupAngles     = E.Settings.StimulusDirection;
[~, minInd]      = min(abs(DirDiff(LookupAngles,PredictedAng)));
whichDir         = minInd;

if range(DisRec) ~= 0  % this is added to avoid scoring no movement as the rightward movement
    if (whichDir == 1)    % i.e. movement in the direction of stimulus.    before rotation was (E.interLoop==whichDir)
        OKNscore =  1;
        %         elseif (whichDir == 5) % i.e. movement in the opposite
        %             OKNscore = -1;
    else
        OKNscore =  0;  % other angle movements
    end
else % i.e. no movement in the signal
    OKNscore =  0;
end

if range(AngRec) ~= 0
    OKNConsistency = (pi-abs(DirDiff(LookupAngles(1),PredictedAng)))/(pi);
else
    OKNConsistency = 0 ;
end

possibleDist     = S.SpeedPixPerMS.*(S.CycleTime.*1000).*2; % the maximum possible movement if just tracking is occured;  Multiplied by two because of saccade it will return
TotalDisRatio    = (nansum(abs(xDall)))./possibleDist;     % total distance eye traveled


% total distance in the stimulus direction
clear ConsistentGainRec ConsistentDistRatioRec
PossFreedomAngles = deg2rad(linspace(0,180,100));
for i = 1: numel(PossFreedomAngles)
    ConsistentAngles = (find(abs(DirDiff(SpeedAng,0))<= PossFreedomAngles(i) )); % difference with zero because each signal is rotated to 0 angle (right-ward mmovement)
    %         figure(22),clf;plot(Tall,Xall);
    %         hold on, plot(Tall(ConsistentAngles),Xall(ConsistentAngles),'go');
    %         title(sprintf('FreedomAngle %2.2f deg', rad2deg(PossFreedomAngles(i))))
    %         drawnow
    ConsitentDist = nansum(xDall(ConsistentAngles));
    theConsistentDistRatio = (ConsitentDist*2) / possibleDist; % times 2 because of saccade
    ConsistentDistRatioRec(i) = theConsistentDistRatio;
    
    theConsGain = nanmean(xDall(ConsistentAngles)) ./ S.SpeedPixPerMS;
    ConsistentGainRec(i)= theConsGain;
end
[ConsistentDistRatio, MaxThetaInd] = max(ConsistentDistRatioRec); % the maximum consistent recorded distance
TheThetaVariablilityRatio = 1-(PossFreedomAngles(MaxThetaInd)/pi);

%     figure(44),plot(PossFreedomAngles, ConsGainRec)
%     figure(45),plot(PossFreedomAngles,ConsistentDistRatioRec)
%     drawnow,KbWait;WaitSecs(0.1);


PursuitOrSaccade =(Speed < Threshold);

clear  PursStartInd PursEndInd

    PursuitOrSaccade = [0 PursuitOrSaccade 0];
    PursStartInd = find(diff(PursuitOrSaccade) == 1);
    PursEndInd   = find(diff(PursuitOrSaccade) == -1);

    LongPusrPieces    = find((PursEndInd-PursStartInd)  > 7); % just keep long trackings to remove short pieces of tracking inside saccades
    PursStartInd = PursStartInd(LongPusrPieces);
    PursEndInd   = PursEndInd(LongPusrPieces);
    LongSaccadePieces   = find((PursStartInd(2:end) - PursEndInd(1:end-1)) > 7); % one in the results means the firt saccade after the first tracking

    try
        PursStartInd = PursStartInd([1 LongSaccadePieces+1]);
        PursEndInd   = PursEndInd([LongSaccadePieces, end]);
        BlinkInd = find(diff(Tall)>1);
        BlinkStartTime = (BlinkInd); % end of good section (margin is removed previously)
        BlinkEndTime = (BlinkInd+1);  % start of good section
        PursStartInd = sort([BlinkEndTime, PursStartInd]);
        PursEndInd = sort([BlinkStartTime, PursEndInd]);

        if lt(TotalDisRatio,0.10) || lt(OKNConsistency,0.10) % means it would not be an OKN
            PursStartInd = PursStartInd(1);
            PursEndInd   = PursEndInd(end);
        BlinkInd = find(diff(Tall)>1);
        BlinkStartTime = (BlinkInd); % end of good section (margin is removed previously)
        BlinkEndTime = (BlinkInd+1);  % start of good section
        PursStartInd = sort([BlinkEndTime, PursStartInd]);
        PursEndInd = sort([BlinkStartTime, PursEndInd]);
        end

    catch
    PursuitOrSaccade = [0 PursuitOrSaccade 0];
    PursStartInd = find(diff(PursuitOrSaccade) == 1);%  & diff(sign([0 SignedSpeed 0]))>=1 );
    PursEndInd   = find(diff(PursuitOrSaccade) == -1);%  & diff(sign([ 0 SignedSpeed 0]))<=-1);

    LongPusrPieces    = find((PursEndInd-PursStartInd)  > 7); % just keep long trackings to remove short pieces of tracking inside saccades
    PursStartInd = PursStartInd(LongPusrPieces);
    PursEndInd   = PursEndInd(LongPusrPieces);
    LongSaccadePieces   = find((PursStartInd(2:end) - PursEndInd(1:end-1)) > 7);
    end


%         figure(6);plot( Tall,Xall,'b',Tall(PursStartInd),Xall(PursStartInd),'k>','MarkerFaceColor','g','MarkerSize',8);
%         hold on, plot(Tall(PursEndInd),Xall(PursEndInd),'r<','MarkerFaceColor','r','MarkerSize',8);ylim([0 E.Window.PTBScreenWidth]), % ,Tall,StimSpeedPlt,'k-.')
%         hold off
%         legend('eye position', 'Star of Pursuit', 'End of Pursuit'),
%         drawnow
% %         KbWait;

        XallStartInd = Xall(PursStartInd);
        XallEndInd   = Xall(PursEndInd);
        YallStartInd = Yall(PursStartInd);
        YallEndInd   = Yall(PursEndInd);
        TallStartInd = Tall(PursStartInd);
        TallEndInd   = Tall(PursEndInd);

        DeltaTallPurs     = TallEndInd - TallStartInd;
        DeltaXallPurs     = XallEndInd - XallStartInd;
        DeltaYallPurs     = YallEndInd - YallStartInd;

        xPursSpeed   = DeltaXallPurs ./ DeltaTallPurs;
        yPursSpeed   = DeltaYallPurs ./ DeltaTallPurs;
        PursSpeed = (xPursSpeed);
        PursSpeedAng = atan2(yPursSpeed,xPursSpeed);

        if length(PursSpeed)>2 && NumGoodSects<=3
            Weight = DeltaTallPurs(2:end-1) ./(sum(DeltaTallPurs(2:end-1)));
            OKNGain = real(exp(nansum(log(PursSpeed(2:end-1)).*Weight)))./ S.SpeedPixPerMS;
        elseif isempty(DeltaTallPurs)
            OKNGain = nan;
        elseif NumGoodSects > 3
            Weight = DeltaTallPurs(3:end-2) ./(sum(DeltaTallPurs(3:end-2)));
            OKNGain = real(exp(nansum(log(PursSpeed(3:end-2)).*Weight)))./ S.SpeedPixPerMS;
        else
            Weight = DeltaTallPurs ./(sum(DeltaTallPurs));
            OKNGain = real(exp(nansum(log(PursSpeed).*Weight)))./ S.SpeedPixPerMS;
        end

        if isempty(DeltaTallPurs)
            OKNGainEstStrength = nan;
        else
            OKNGainEstStrength = numel(Weight)./numel(DeltaTallPurs);
        end



% the moving mean gain with constand threshold

% % ConsPursuits = Speed(Speed<=ConsThreshold);
% MovMeanPursuit = movmean(ConsPursuits,300);
% MMGain = nanmean(MovMeanPursuit ./ S.SpeedPixPerMS);
% MMGainSD = nanstd(MovMeanPursuit ./ S.SpeedPixPerMS);
% MMGainSEM = MMGainSD./(sqrt(numel(MovMeanPursuit)));

% sliding speed gain not working yet

%SSpdWS = 10; % Slide Speed window size for befor and after the point
ThePursuitInds = find(abs(xDall) < Threshold); % & xDall>=0);
ThePursuitX = Xall(ThePursuitInds+1);  % this way we loose the first milisecond
PadXall = padarray(ThePursuitX,[0 SSpdWS],'replicate','both');
PadTall = padarray(Tall(ThePursuitInds),[0 SSpdWS*2],'replicate','post'); % post because time start from 0 and we can not add 'pre'
clear SlidSpd AllFitCoef
% SlidSpd = []; AllFitCoef=[];
SlidSpd = [] ; % zeros(size(ThePursuitX));
clear figure(88)
% SamplingRate = 1;
for i=(SSpdWS+1):SamplingRate:length(PadXall)-(SSpdWS) % to generaate n from the first original data point to the end point in the original data
    %     clear n
    n = i-SSpdWS; % the n-th point of the output signal
    TheSpeedWin = (PadXall(i+SSpdWS) - PadXall(i-SSpdWS))./ (PadTall(i+SSpdWS) - PadTall(i-SSpdWS)) ; %SSpdWS; % nanmean(diff(PadXall(i-SSpdWS:i+SSpdWS)))(PadXall(i+SSpdWS+1) - PadXall(i-SSpdWS))
    SlidSpd(n) = TheSpeedWin ; %
    %     SlidSpd = [SlidSpd, TheSpeedWin];
    
    %     figure(999), plot(PadTall,PadXall,'g.'),hold on
    %     plot(PadTall((i-SSpdWS):(i+SSpdWS)),PadXall(i-SSpdWS:i+SSpdWS),'b.')
    %     hold off
    %     drawnow
    %
    % try glm fit
    %     if diff(PadXall((i-SSpdWS):(i+SSpdWS))) < Threshold % i.e when all point inside a window is tracking do the analysis
    %     [b,dev,stats] = glmfit(PadTall(i-SSpdWS:i+SSpdWS),PadXall(i-SSpdWS:i+SSpdWS));
    %     AllFitCoef(i,:,:) = b; % [AllFitCoef; b];
    %     yfit = glmval(b,PadTall(i-SSpdWS:i+SSpdWS),'identity');
    %     else
    %     AllFitCoef(i,:,:) = [nan nan];
    %     end
    %     if lt(abs(b(2)),Threshold)
    %         Colsrt = 'b.';
    %     else
    %         Colsrt = 'r.';
    %     end
    %     figure(88),
    %     plot(PadTall,PadXall,'g.')
    %     hold on
    %     ls = plot(PadTall(i:SSpdWS+i),yfit,Colsrt);
    %     drawnow
    %     set(ls,'LineWidth',1.5)
    %     hold off
    %     % glm fit finished
end


SlidSpd = SlidSpd((abs(SlidSpd) < Threshold)); % & (SlidSpd>=0));

% SldGain   = exp(nanmean(log(SlidSpd./ S.SpeedPixPerMS))) ;
SldGain   = (nanmean((SlidSpd./ S.SpeedPixPerMS))) ;

% Spdd = AllFitCoef(:,2);
% purs = Spdd(abs(Spdd)<ConsThreshold); %  & Spdd >=0);
% theFitGain = mean(purs)./S.SpeedPixPerMS;

simpleGain = nanmean(xDall(ThePursuitInds)) ./ S.SpeedPixPerMS;



%%  using find peaks
% clear TroughInds PeakInds DeltaTallPurs DeltaXallPurs
% PadLength = 0;
% XallPad = padarray (Xall, [0 PadLength], 'replicate','both');
% [PeakVal, PeakInds,PeakW,PeakP] = findpeaks(XallPad,'MinPeakProminence',15,'MinPeakDistance',100);%,Tall,'MinPeakDistance',6);
% PeakInds = PeakInds-PadLength;
% 
% XallFlipedUpDown =  (-(XallPad(:) - E.Window.PTBScreenWidth))';
% XallFlipedUpDownPad = padarray (XallFlipedUpDown, [0 PadLength], 'replicate','both');
% 
% [TroughVal, TroughInds, TroughW, TroughP] = findpeaks(XallFlipedUpDownPad,'MinPeakProminence',15,'MinPeakDistance',100); %, Tall,'MinPeakDistance',6);
% TroughInds =TroughInds-PadLength;
% 
% % if NumGoodSects>1
% BlinkInd = find(diff(Tall)>1);
% BlinkStartTime = (BlinkInd); % end of good section (margin is removed previously)
% BlinkEndTime = (BlinkInd+1);  % start of good section
% if size(TroughInds,1)~=1
%     TroughInds = TroughInds';
% end
% if size(PeakInds,1)~=1
%     PeakInds = PeakInds';
% end
% if ~ismember(BlinkEndTime, TroughInds)
% PursStartInd = sort([BlinkEndTime, TroughInds]);
% else
% PursStartInd = sort([1, TroughInds ]);
% end
% if ~ismember(BlinkStartTime, PeakInds)
% PursEndInd   = sort([BlinkStartTime, PeakInds]);
% else
% PursEndInd = sort([PeakInds , length(Tall)]);
% end
% % else
% %      PursStartInd = TroughInds';
% %      PursEndInd   = PeakInds;
% % end
% 
% if lt(TotalDisRatio,0.10) || lt(OKNConsistency,0.10) % means it would not be an OKN
%     PursStartInd = PursStartInd(1);
%     PursEndInd   = PursEndInd(end);
% end
% XallStartInd = Xall(PursStartInd);
% XallEndInd   = Xall(PursEndInd);
% %         YallStartInd = Yall(PursStartInd);
% %         YallEndInd   = Yall(PursEndInd);
% TallStartInd = Tall(PursStartInd);
% TallEndInd   = Tall(PursEndInd);
% 
% if TallEndInd(1)>TallStartInd(1)
%     for i = 1: min([length(TallStartInd) , length(TallEndInd)])
%         DeltaTallPurs(i)     = TallEndInd(i) - TallStartInd(i);
%         DeltaXallPurs(i)     = XallEndInd(i) - XallStartInd(i);
%     end
% else
%     for i = 1: min([length(TallStartInd) , length(TallEndInd)])-1
%         DeltaTallPurs(i)     = TallEndInd(i+1) - TallStartInd(i);
%         DeltaXallPurs(i)     = XallEndInd(i+1) - XallStartInd(i);
%     end
% end
% 
% %         DeltaYallPurs     = YallEndInd - YallStartInd;
% 
% xPursSpeed   = DeltaXallPurs ./ DeltaTallPurs;
% %         yPursSpeed   = DeltaYallPurs ./ DeltaTallPurs;
% PursSpeed = (xPursSpeed);
% %         PursSpeedAng = atan2(yPursSpeed,xPursSpeed);
% 
% % if length(PursSpeed)>2  && NumGoodSects<=3
% %     Weight = DeltaTallPurs(2:end-1) ./(sum(DeltaTallPurs(2:end-1)));
% %     OKNGain = real(exp(nansum(log(PursSpeed(2:end-1)).*Weight)))./ S.SpeedPixPerMS;
% % elseif isempty(DeltaTallPurs)
% %     OKNGain = nan;
% % elseif NumGoodSects > 3
% %     Weight = DeltaTallPurs(3:end-2) ./(sum(DeltaTallPurs(3:end-2)));
% %     OKNGain = real(exp(nansum(log(PursSpeed(3:end-2)).*Weight)))./ S.SpeedPixPerMS;
% % else
% %     Weight = DeltaTallPurs ./(sum(DeltaTallPurs));
% %     OKNGain = real(exp(nansum(log(PursSpeed).*Weight)))./ S.SpeedPixPerMS;
% % end
% 
% if isempty(DeltaTallPurs)
%     OKNGain = nan;
% else
%     Weight = DeltaTallPurs ./(sum(DeltaTallPurs));
%     OKNGain = real(exp(nansum(log(PursSpeed).*Weight)))./ S.SpeedPixPerMS;
% end
% 
% if isempty(DeltaTallPurs)
%     OKNGainEstStrength = nan;
% else
%     OKNGainEstStrength = numel(Weight)./numel(DeltaTallPurs);
% end

% using MAD to calculate gain %still working on it
AllPursSpeed = xDall(ThePursuitInds);
% PurSpeedMAD = median(abs(AllPursSpeed-median(AllPursSpeed)));

% if OKN 
% if OKNscore == 1
%     NonZeroInds = find((abs(xDall)>0.1) && (abs(xDall) < Threshold));
%     AllnonzeroPursSpeed = AllPursSpeed(NonZeroInds);
%     OKNSimpleGain = AllnonzeroPursSpeed./ S.SpeedPixPerMS;
%     MADgain = median(abs(OKNSimpleGain-median(OKNSimpleGain)));
% else
    theGain = AllPursSpeed./ S.SpeedPixPerMS;
    MADgain = median(abs(theGain-median(theGain)));
% end

% 
% figure(7), plot(Tall,Xall,'b.',Tall(PursStartInd),Xall(PursStartInd),'go', Tall(PursEndInd),Xall(PursEndInd),'r*','MarkerSize',9,'LineWidth',2)
% % %  hold on , plot(tData,xData,'r-'),shg % clipped raw data
% rfg = refline(SldGain.*S.SpeedPixPerMS,0);set(rfg,'Color','g','LineWidth', 2, 'LineStyle','-.')
% rfs = refline(S.SpeedPixPerMS,0);set(rfs,'Color','m','LineWidth', 2, 'LineStyle','-.')
% 
% legend('All X data', 'Purs Start Point','Purs End Point','Gain Refrence','Stim Speed Refrence','Location','best')
% title(sprintf('OKN Gain = %2.2f',SldGain))
% pause