% % function [E] = OFR_DurationSignalQualityControl(E,P)
% % have to initialize new structures
% % theEye = 2 ; % 1 = left; 2 = right 
% % coimmented out to deal with timing problem first
% intLe           = P.InterList(P.CurrentTrialNo);
% intLeTrial      = P.TrialNumbers(P.CurrentTrialNo);
% theEye = E.WhichEye;
% % theEye = (1:2);
% 
% StimulusDirection = DegToRad(E.Settings.StimulusDirection(intLe));
% 
% SpeedQualityFlag =[];
% 
%  ThisTrialX = squeeze(E.EyePos.X(intLe,intLeTrial,:,theEye)).* X2PixConvertRatio;
%  ThisTrialY = squeeze(E.EyePos.Y(intLe,intLeTrial,:,theEye)).* Y2PixConvertRatio;
%  ThisTrialPA = squeeze(E.EyePos.PA(intLe,intLeTrial,:,theEye));
%  ThisTrialTime      = squeeze(E.EyePos.Time(intLe,intLeTrial,:));
%  
% % rotate the signal
% ThisTrialX0     = ThisTrialX - E.Window.PTBScreenWidthCenter; % change the center of rotation to the center of data
% ThisTrialY0     = ThisTrialY - E.Window.PTBScreenHeightCenter;  %  E.Window.PTBScreenWidthCenter
% 
% % rotate the signal. Note the input angle should be negative here
% ThisTrialX = ThisTrialX0.*cos(-StimulusDirection) - ThisTrialY0.*sin(-StimulusDirection);
% ThisTrialY = ThisTrialX0.*sin(-StimulusDirection) + ThisTrialY0.*cos(-StimulusDirection);
% 
% ThisTrialX = ThisTrialX + E.Window.PTBScreenWidthCenter; % change back the center to the original coordinate center %   E.Window.PTBScreenWidth;
% ThisTrialY = ThisTrialY + E.Window.PTBScreenHeightCenter;%   E.Window.PTBScreenHeight;
%  
%  
%  ThisTrialInterpolatedFrames = sum(E.Window.InterpolatedFrames(intLe,intLeTrial,:));
%  ThisTrialFlipTime =  squeeze(E.Window.FrameFlipTime(intLe,intLeTrial, :));
%  ThisTrialFlipTimeFreq = 1./diff(ThisTrialFlipTime);
%  % clipping off end of sequence
% %  ThisTrialPA = ThisTrialPA(ThisTrialTime>0); % remove time vector's zero tail before processing
% %  ThisTrialX  = ThisTrialX(ThisTrialTime>0);
% %  ThisTrialY  = ThisTrialY(ThisTrialTime>0);
% %  ThisTrialTime = ThisTrialTime(ThisTrialTime>0);
%  
% 
%  
% % first sample to check Gaussian fit-ablility
% ThisTrialEachFrameSamples = (squeeze(E.EyePos.OverSample(intLe, intLeTrial, :,:)))'; % keep the frame flip time for later processing
% NoSampsPerFrame = sum(ThisTrialEachFrameSamples>0);
% EachFrameSamplesGT0 = (ThisTrialEachFrameSamples(ThisTrialEachFrameSamples>0))'; % length(EachFrameSamples)== length(xData0)? : yes
% FSInds  = find(EachFrameSamplesGT0==1);
%  tDataDiff = diff([ 0 ThisTrialTime' 0]);
%  FSInds    = [1, find(tDataDiff>E.Settings.MaxSamplesPerFrame.*2)]; % FS: first Sample.  *2 due te 500Hz sampling rate
%        
% ThisTrialFSX =ThisTrialX(FSInds,theEye);
% ThisTrialFSY =ThisTrialY(FSInds,theEye);
% ThisTrialFSPA =ThisTrialPA(FSInds,theEye);
% ThisTrialFSTime =ThisTrialTime(FSInds);
% 
% % goodFSpupil = ThisTrialFSPA>0 ;
% % ThisTrialFSX = ThisTrialFSX(goodFSpupil);
% % ThisTrialFSY = ThisTrialFSY(goodFSpupil);
% % ThisTrialFSPA = ThisTrialFSPA(goodFSpupil);
% % ThisTrialFSTime =ThisTrialTime(goodFSpupil);
% 
% % Blinks and "no data" are nan in Tobii
% NotisNaNsNumber = sum(isnan([ThisTrialFSX(:,theEye); ThisTrialFSY(:,theEye)]));
% % NotisNaNsNumber = sum(([ThisTrialFSX(:,theEye); ThisTrialFSY(:,theEye)])>0);
% GoodGazeRatio = mean((NotisNaNsNumber(theEye)./(length(ThisTrialFSX)*2)));%theEye
%  
% NoBins=16;
% SpeedEsts = diff(ThisTrialFSX(:,theEye));
% TrackSpeedEsts = SpeedEsts(SpeedEsts(:,theEye)>0);
% 
% try
% [SpFreq, SpBins]=hist(log(TrackSpeedEsts),NoBins);
% SpeedGoodPropRatio = sum(SpFreq~=0)./NoBins;
% SpeedQualityFlag = SpeedGoodPropRatio>= 0.5; 
% catch
%     SpeedQualityFlag = false;
% end
% 
% % calculate the amount of good head position in this trial
% E.GoodHeadPositionRatio = sum(E.GoodHeadPositionFlags(intLe,intLeTrial,:))./ length(E.GoodHeadPositionFlags(intLe,intLeTrial,:));
% 
%  E.TrialDropedFrames = sum(ThisTrialFlipTimeFreq<57);
%  E.TrialGoodDataRatio = GoodGazeRatio;%(numel(ThisTrialGoodPA)./numel(ThisTrialPA));%(intLe,intLeTrial)
%  E.TrialGoodDataFlag  = (E.TrialGoodDataRatio >= 0.80) & (ThisTrialInterpolatedFrames <= 5) & (E.TrialDropedFrames<2) & (SpeedQualityFlag) & (E.GoodHeadPositionRatio > 0.7); % acc good data hiher than  80 percent and no more than one droped frames
E.TrialGoodDataFlag = true; % hardcoded to check other parts of the main code
E.TrialGoodDataRatio = 1;