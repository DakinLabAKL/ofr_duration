% OFR_DurationInit
clearvars;
E.AddTask{1}='PsychImaging(''AddTask'', ''FinalFormatting'', ''DisplayColorCorrection'', ''SimpleGamma'')';
E = PsychSandboxInitialiseEquipment(E); % initialise equipment
E.Settings.ExperimentSet = 1; % 1 = pupil ,easurement and VFL simulation 2 = 100 and 50 percent field and various contrast contrast levels
E.Window.ScreenHz=round(1/E.Window.InterFrameInterval);

switch E.Settings.ExperimentSet
    case 1 % i.e pupil measurement and VFL study
 S.SpeedDegPerSec  = 2; %   % default: 10
       E.Settings.PixelsPerCm      = 37.6; % 
        P.PixelsPerDegree           = 40; % HardCoded. 24 @1280 pix and 70cm viewing sidtance; 36@ 1920 pix res; % 72@3840      %120; % the actual pixels perdegree is 120 for horizontal and 114.5 for vertical Dell 4k monitor
        E.Settings.TestDistCm       = (0.5*(P.PixelsPerDegree/E.Settings.PixelsPerCm)) / tand(0.5);           %700; % 1000;   % test distance in mm. distance between eye and LCD
        S.DirectionNums     = 2;      % number of test directions 8,4 or 2. Start from 2AFC -->4AFC -->8AFC (turn on NumLOCK for 8AFC)
        S.BaseMaskLevels    = 5;% [1/2, 2]; % Circular mask diameter in degrees
        S.BaseContrastLevels = 0.95;%[0.025 0.95]; % 2.^[-6:0];%[0. 0.0625 0.125 0.25 0.5 1.00];%[0.0156  0.0449   0.1250   1.0000]; % inf (1/512).*round(2.^[3:1.5:10])   %0.125; % 0.35; % 10.^ (linspace(-2,0,5)); % from lowest contrast to hieghest contrast
        E.FixationOnOff = [0]; % 0 is off, 1 is on
        S.MaskType          = 1;     % 1 = complete circular mask    2 = ring shape circular mask (showing the stim on the doughnut area) 3 = square mask 4 = noise mask foveal visible 5 =  noise patch with occluded foveal region
        S.MaskOpenLoop      = 0;
        S.StimulusDirection = rad2deg(0:pi/(S.DirectionNums /2):(2*pi-pi/(S.DirectionNums /2))); % [ 0    45   90   135  180  225   270  315];
        S.StimFreqCPDs      =  2; % Stimulus frequency cycle per degree; Dell S2817Q @ 1m is  18.8807 x  31.8816 degrees
        P.LocalTrialNo       = 1; %4;  % number of repeats for each condition
        E.Settings.IsFixationMarker   = [0 1]; % 0: no fixation marker 1: fixation marker on stimulus
        E.WhichEye = [1:2]; % 2 is right eye
        E.Window.MaskBackground = E.Window.Gray;%  E.Window.PTBbackground; % Background color of mask
        P.ThreshEstProcedure = 'QUEST';
        P.NoAFC = 1;
        P.NoInterleaved = length(S.BaseMaskLevels)*length(S.BaseContrastLevels);
         
        %%%%% no trials %%%%%
        P.NoFixedTrialsPerInter     = 8.*ones(1,P.NoInterleaved);

         P.ThreshUnits = 'RAWQUEST';
        P.pThreshold=0.75;
        P.beta=3.5;
        P.LapseRate=0.01;
        P.gamma=0.5;
        P.ThreshPriorGuess=2.*ones(1,P.NoInterleaved);
        P.ThreshPriorGuessSD=2.*ones(1,P.NoInterleaved);
end

E.Settings.MaskNo            = length(S.BaseMaskLevels); % 9;     % number of different mask sizes
HideCursor;
%E.Settings.TotalTrialPerCon = length(E.Settings.BaseContrastLevels).* E.Settings.DirectionNums .* length(E.Settings.StimFreqCPDs).*E.Settings.MaskNo .* E.Settings.LocalTrialNo .* numel(E.Settings.IsFixationMarker);

P.StimulusDirection = (S.StimulusDirection);

if     S.DirectionNums == 2
    P.Responses     =  {'6'  '4'}; % upper({'RightArrow' 'LeftArrow'});
elseif S.DirectionNums == 4
    P.Responses     = {'6'  '8'   '4'   '2'  }; %upper({'RightArrow' 'UpArrow' 'LeftArrow' 'DownArrow'});
else  %i.e. 8
    P.Responses     =  {'6'  '9'   '8'  '7'  '4'  '1'   '2'  '3' }; % use keypad numlock on  i.e. {'6':Right  '9':UpperRight   '8':Up  '7': UpLeft  '4':Left  '1':DownLeft  '2':Down  '3':DownRight };
end
P.RepeatKey={'r'};

% the first mask is the largest portion of visible stimulus and the last mask
% is the lowest portion of the visible stimulus
% P.PossStimValues             = repmat({E.Settings.StimFreqCPDs}, [ 1 E.Settings.DirectionNums]);
% P.PossMaskValues             = repmat({S.BaseMaskLevels}, [ 1 E.Settings.DirectionNums]);
% P.PossContrastValues         = repmat({E.Settings.BaseContrastLevels}, [ 1 E.Settings.DirectionNums]);
P                            = PsychSandboxInitialisePsychophysics(P); % no argument = make from scratch


S.AudioFeedback          = 0; %1=yes, provide audio feedback please, 0=no, no feedback
S.VisualFeedback         = 0; %1=yes, provide visual feedback please, 0=no, no feedback
S.TextVisualFeedBack     = 0;  %1=yes, provide text visual feedback please, 0=no, no feedback
S.PepperMode             = 0;
E.Settings.CoveredFrames = 0; %30; % number of frames to be covered at the beginnig to avoid jittering frames
S.CycleTime              = 1;%2;  % stimulus presenting time s
S.StimulusLength         = (S.CycleTime + E.Settings.CoveredFrames.*E.Window.InterFrameInterval).*ones(P.NoInterleaved,max(P.NoFixedTrialsPerInter)); % IN seconds would be 3 s for 60 frames initial covering
S.DirPeak                = P.StimulusDirection; %[0 180];%[0 0 0 0 0 180 180 180 180 180] ; % [0 90 180 270];
S.NoSamps                = 1; %2; % stim pattern samples
S.NoMaskSamps            = 1; %8; % number of different noise samples for the same mask properties
S.WhichSamp              = Randi(S.NoSamps,[1 sum(P.NoFixedTrialsPerInter)]);
S.WhichMaskSamp          = Randi(S.NoMaskSamps,[1 sum(P.NoFixedTrialsPerInter)]);

E.Window.PTBScreenWidthCenter  = E.Window.PTBScreenWidth/2;
E.Window.PTBScreenHeightCenter = E.Window.PTBScreenHeight/2;
E.RepeatKeyPressed = 0; % % any key other than enswer keys is repeat

P.BaseTrialNo = 0; % will count the number of baseline pupil measurements

S.StimScalingRatio = 1; %8;% 4 % 4 means generate the stimulus using 1/4th of PTB screen dimensions.
if S.MaskOpenLoop
    S.MaskSizeMltpl = 1.7; % the interger for mask to be multipled with
    S.StimX    = ceil(E.Window.PTBScreenWidth/S.StimScalingRatio); S.StimY=ceil(E.Window.PTBScreenHeight/S.StimScalingRatio); % the stimilus size
    S.DestRect = CenterRectOnPoint([0 0 E.Window.PTBScreenWidth E.Window.PTBScreenHeight],E.Window.PTBScreenWidthCenter,E.Window.PTBScreenHeightCenter); % the destination rect in which we flip the Stim
else
    S.MaskSizeMltpl = 1; % the interger for mask to be multipled with
    S.StimX    = ceil(E.Window.PTBScreenWidth/S.StimScalingRatio); S.StimY=ceil(E.Window.PTBScreenHeight/S.StimScalingRatio); % the stimilus size; square.
    S.DestRect = CenterRectOnPoint([0 0 E.Window.PTBScreenWidth E.Window.PTBScreenHeight],E.Window.PTBScreenWidthCenter,E.Window.PTBScreenHeightCenter); % the destination rect in which we flip the Stim
end

S.FlipsPerFrame   = 1;
S.gamVal          = 0.5;
S.sfBw            = 0.5;
S.ShowMovies          = 0;
JumpSizePerFrameInDeg = (S.SpeedDegPerSec*E.Window.InterFrameInterval)*S.FlipsPerFrame ;
S.JumpSize            = JumpSizePerFrameInDeg*P.PixelsPerDegree; % jump size pixels/frame
S.SpeedPixPerSec = P.PixelsPerDegree.*S.SpeedDegPerSec; % stim speed in pixel per second S.SpeedPixPerMS
S.SpeedPixPerMS  = S.SpeedPixPerSec./1000; % Strim speed in pixel per milisecond
S.ElementCode          = 8;
S.BaseNo               = 1024;
S.SmallestElementSize  = 16; % in pixels
S.CPI                  = 8;
E.Settings.TimingTolerance   = 0.7;  % we have overwritten the value in PSB for this experiment
E.Timing.EyetrackingRiskRate = 0.3;         % we have overwritten the value in PSB for this experiment

E.Window.PTBbackground = E.Window.Gray;
if  strcmp( E.Eyetracker,'Tobii')
    X2PixConvertRatio = E.Window.PTBScreenWidth;
    Y2PixConvertRatio = E.Window.PTBScreenHeight;
else
    X2PixConvertRatio = 1;
    Y2PixConvertRatio = 1;
end
E.Window.DisplayHeightCm       = 29; % measured diplay heigth E.Window.DisplayHightmm this values measured for dell S2817Q 4k display 341
E.Window.DisplayWidthCm        = 51; % measured diplay width E.Window.DisplayHightmm dell S2817Q 4k 621
E.Window.LastFrameDelta        = 1; % it is better to put in psb init
E.Window.DisplayHeightDeg      = atand(E.Window.DisplayHeightCm / E.Settings.TestDistCm); % calculate the screen hieght in degrees for the desired test distance. (i.e. = S.screenHeightDeg ) The S2817Q height is 34.128 cm and its width is 62.093 based on www.displayspecifications.com and the screen manual
E.Window.DisplayWidthDeg       = atand(E.Window.DisplayWidthCm / E.Settings.TestDistCm);
E.Window.DisplayPixleHeightArcMin = E.Window.DisplayHeightDeg / E.Window.PTBScreenHeight*60; % *60 to convert from degree to ArcMin
E.Window.DisplayPixleWidthArcMin  = E.Window.DisplayWidthDeg / E.Window.PTBScreenWidth*60;
for c = 1:length(S.BaseContrastLevels)
    S.FixedContrast = S.BaseContrastLevels(c);
    StimFreqCPIs = S.StimFreqCPDs.* E.Window.DisplayHeightDeg; % stimulus frequency in cycles per image height
    lambdaPix  = E.Window.PTBScreenHeight./S.StimFreqCPDs;  %S.FreqPeak
    Smoothing  = 1.*lambdaPix; %
    srcNoise   = randn(S.StimY,S.StimX);
   % BaseIm=real(DoLogGabor(srcNoise,StimFreqCPIs,S.sfBw,0,10000));
   lambda= S.StimY/StimFreqCPIs;
    BaseIm=GenerateGabor(S.StimX,S.StimY, 1000000,1000000, pi/2, pi/2, lambda,0,0,0);%      srcNoise,StimFreqCPIs,S.sfBw,0,10000));
    stimImIn   = (NormImage(BaseIm,0,0.5));% DegToRad(2)
    stimIm=0.5+NormImage(stimImIn,0,0.5*S.FixedContrast);
    theImage=repmat(stimIm,[2 2]);
    theImages = double(theImage); %(f,c,s,:,:)
   S.StimTex(c)       = Screen('MakeTexture', E.Window.Pointer, theImage, [], [], 2);
end
%S.StimTex = stimtex;
mask1=zeros(round(S.StimY),round(S.StimX),2);
%%
S.MaskCons=[0:100]./100;
for MaskSample = 1: S.NoMaskSamps
    DisplayAreaDeg =  E.Window.DisplayHeightDeg*E.Window.DisplayWidthDeg;
    for MaskLoop = 1: numel(S.BaseMaskLevels)
        MaskRadDeg = S.BaseMaskLevels(MaskLoop)/2; % mask radius (deg)
        MaskRadPix = (P.PixelsPerDegree.* MaskRadDeg/(S.MaskSizeMltpl.*S.StimScalingRatio));
        % res  =
        % MakeCosineRectWindow(size(mask1,2),size(mask1,1),MaskRadPix/(1.*S.MaskSizeMltpl.*S.StimScalingRatio),0,0,0);
        % % scrMinDim/2.1
        res  = GenerateGaussian(size(mask1,2),size(mask1,1),MaskRadPix,MaskRadPix, 0,0,0); %   MaskRadPix/(S.MaskSizeMltpl.*S.StimScalingRatio),0,0,0);   % scrMinDim/2.1
        res(res<0.01)=0.;
        P.ActualVisibleRatio(MaskLoop) = sum((1-res(:))==0)./ sum((1-res(:))==1);
        figure(12);
        subplot(numel(S.BaseMaskLevels),1,MaskLoop);imshow(res)
        title(sprintf('rad is %3.3f',MaskRadPix))
        mask1(:,:,1)= E.Window.MaskBackground; %E.Window.MaskBackground;%E.Window.Gray;
        for mm=1:length(S.MaskCons)
            mask1(:,:,2)=  E.Window.White.*(1-res.*S.MaskCons(mm));
            mask1=squeeze(mask1(:,:,:));
            S.masktex(MaskLoop,MaskSample,mm)=Screen('MakeTexture', E.Window.Pointer,mask1);
        end
    end
end

E.Window.GamVal          = 0.5; % calibrated for S2817Q under game mode, (75% brightness and 75% contrast)
PsychColorCorrection('SetEncodingGamma', E.Window.Pointer, E.Window.GamVal );



%%%%%%%%%%%%%%%%% call PSB update  %%%%%%%%%%%%%%%%%
[E,P,S]      = PsychSandboxUpdate(E,P,S);   % Now that S is known

E.EyePos.LE_Gazeorigin = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial,3, 'single'); % 3 because of x,y,z
E.EyePos.RE_Gazeorigin = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial,3, 'single');
E.GoodHeadPositionFlags = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial, 'single');
%P.InterList  = repmat(P.theInterList(1,:),[1 P.MaxTrialsPerInter(1)]); % sort(P.InterList)


[MaskNos ContrastNos] = ndgrid([1:length(S.BaseMaskLevels)],[1:length(S.BaseContrastLevels)])
MaskNos=All(MaskNos)';
ContrastNos=All(ContrastNos)';
for tri=1:P.TotalTrials
    thisInter=P.InterList(tri);
    P.MaskInds(tri)=MaskNos(thisInter);
    P.ContrastInds(tri)=ContrastNos(thisInter);
end

        S.StimNumber=Randi(length(S.DirPeak),[P.NoInterleaved max(P.NoFixedTrialsPerInter)]);
  %  S.StimNumber(thisInter,localTrialNo)=Randi(length(S.DirPeak),[]);
S.ThisTrialDir = pi-DegToRad(S.DirPeak(S.StimNumber));


%
%  [InterList TriaList] = ndgrid([1:P.NoInterleaved],[1:P.NoFixedTrialsPerInter]);
%  InterList=All(InterList)';
%  TriaList=All(TriaList)';

% [~, indices]=Shuffle(InterList);
% P.InterList = InterList(indices);
% P.MaskInds   = MaskNos(indices);
% P.ContrastInds=ContrastNos(indices);
% P.FixMrkInds = FixationsNos(indices);



% [bInters, bStimInd, bMaskInd, bRepeat, bCon, bFix]=ndgrid((1:P.NoInterleaved),(1:numel( E.Settings.StimFreqCPDs)),(1:E.Settings.MaskNo),(1:E.Settings.LocalTrialNo),(1:length(P.PossContrastValues{1})), E.Settings.IsFixationMarker);
% bInters=All(bInters);
% bStimInd = All(bStimInd);
% bMaskInd=All(bMaskInd);
% bRepeat=All(bRepeat);
% bCon=All(bCon);
% bFix = All(bFix);
%
% [~, indices]=Shuffle(bInters);
% P.InterList=bInters(indices)';
% P.StimInds=bStimInd(indices)';
% P.ContrastInds=bCon(indices)';
% P.MaskInds   = bMaskInd(indices)';
% P.FixMrkInds = bFix(indices)';

% P.StimLevels = P.PossStimValues{1}(P.StimInds);
% P.MaskLevels = P.PossMaskValues{1}(P.MaskInds);
% P.ContrastLevels = P.PossContrastValues{1}(P.ContrastInds);
P.ResponseKeyName  = cell(1, P.TotalTrials);


KbReleaseWait