function [E,status] = OFR_DurationSimpleTimingGetBehaviour(E,P)

%local variables
% entranceTime    = GetSecs;
numETSamps      = uint8(0);
intLe           = P.InterList(P.CurrentTrialNo);
intLeTrial      = P.TrialNumbers(P.CurrentTrialNo);
sampleCount     = E.SamplesPerTrial(intLe,intLeTrial);

E.Window.FrameCount(intLe,intLeTrial) = E.Window.FrameCount(intLe,intLeTrial) + 1; %may not (but ideally should) be true frames as flip may not occur, just function calls
frameThisTrial  = E.Window.FrameCount(intLe,intLeTrial);

E.Window.LastFlipTime = Screen('Flip' , E.Window.Pointer, (E.Window.LastFlipTime + 0.5 * E.Window.InterFrameInterval)); % ok if halting, as first

if strcmpi(E.Eyetracker,'Tobii') && E.Computer.TobiiConnected
    TobiiSample = E.Settings.Tobii.ET.get_gaze_data('flat');
    sampleCount = sampleCount + 1;
    if isempty(TobiiSample.left_gaze_point_on_display_area) && isempty(TobiiSample.right_gaze_point_on_display_area)
        E.EyePos.X(intLe,intLeTrial,sampleCount,:)           = [E.LastEyePos(1) E.LastEyePos(1)]; %
        E.EyePos.Y(intLe,intLeTrial,sampleCount,:)           = [E.LastEyePos(2) E.LastEyePos(2)]; % [0,0];
        E.EyePos.PA(intLe,intLeTrial,sampleCount,:)          = [nan nan];
        E.EyePos.Time(intLe,intLeTrial,sampleCount)          =  nan;
        status = 'No Sample';
    elseif isempty(TobiiSample.left_gaze_point_on_display_area)
        E.EyePos.X(intLe,intLeTrial,sampleCount,:)           = [E.LastEyePos(1) TobiiSample.right_gaze_point_on_display_area(end,1)]; %
        E.EyePos.Y(intLe,intLeTrial,sampleCount,:)           = [E.LastEyePos(2) TobiiSample.right_gaze_point_on_display_area(end,2)]; % [0,0];
        E.EyePos.PA(intLe,intLeTrial,sampleCount,:)          = [nan TobiiSample.right_pupil_diameter(end)];
        E.EyePos.Time(intLe,intLeTrial,sampleCount)          = double(TobiiSample.device_time_stamp(end));
        status = 'No LE Sample';
    elseif isempty(TobiiSample.right_gaze_point_on_display_area)
        E.EyePos.X(intLe,intLeTrial,sampleCount,:)           = [TobiiSample.right_gaze_point_on_display_area(end,1) E.LastEyePos(1)]; %
        E.EyePos.Y(intLe,intLeTrial,sampleCount,:)           = [ TobiiSample.right_gaze_point_on_display_area(end,2) E.LastEyePos(2)]; % [0,0];
        E.EyePos.PA(intLe,intLeTrial,sampleCount,:)          = [nan TobiiSample.right_pupil_diameter(end)];
        E.EyePos.Time(intLe,intLeTrial,sampleCount)          = double(TobiiSample.device_time_stamp(end));
        status = 'No RE Sample';
    else
        %NB Tobii returns singles as default
        E.EyePos.X(intLe,intLeTrial,sampleCount,:)           = [TobiiSample.left_gaze_point_on_display_area(end,1),TobiiSample.right_gaze_point_on_display_area(end,1)];
        E.EyePos.Y(intLe,intLeTrial,sampleCount,:)           = [TobiiSample.left_gaze_point_on_display_area(end,2),TobiiSample.right_gaze_point_on_display_area(end,2)];
        E.EyePos.PA(intLe,intLeTrial,sampleCount,:)          = [TobiiSample.left_pupil_diameter(end) TobiiSample.right_pupil_diameter(end)];
        E.EyePos.Time(intLe,intLeTrial,sampleCount)          = double(TobiiSample.device_time_stamp(end));
        status = 'OK';
        OFR_DurationHeadPositionControl
        E.LastEyePos = [E.EyePos.X(intLe,intLeTrial,sampleCount),E.EyePos.Y(intLe,intLeTrial,sampleCount)];
    end
    numETSamps = numETSamps + 1;
    E.EyePos.OverSample(intLe,intLeTrial,frameThisTrial,numETSamps)       = numETSamps;
end
% if frameThisTrial == 1 %the first frame of a trial has to be good
% E.Window.LastFlipTime = Screen('Flip' , E.Window.Pointer, (E.Window.LastFlipTime + 0.5 * E.Window.InterFrameInterval)); % ok if halting, as first
%     E.Window.LastFrameDelta = 1;
% else
%     E.Window.LastFrameDelta = 1;
%     if entranceTime < (E.Window.LastFlipTime + (E.Window.InterFrameInterval * 0.5))% E.Settings.TimingTolerance)) % If have enough time
%         E.Window.LastFlipTime = Screen('Flip' , E.Window.Pointer, (E.Window.LastFlipTime + (1-E.Settings.TimingTolerance) * E.Window.InterFrameInterval)); % here we flip the stimulus to the screen E.Timing.EyetrackingRiskRate
%     else % if we don't have enough time
%         E.Window.LastFrameDelta = E.Window.LastFrameDelta + 1;
%         E.Window.InterpolatedFrames(intLe,intLeTrial,frameThisTrial) = 1; %E.Window.LastFrameDelta-1;
%         E.Window.LastFlipTime = E.Window.LastFlipTime + (E.Window.InterFrameInterval * E.Settings.TimingTolerance);
% %     end
% end
E.Window.FrameFlipTime(intLe,intLeTrial,frameThisTrial)= E.Window.LastFlipTime;
E.Window.FrameDelta(intLe,intLeTrial,frameThisTrial)   = E.Window.LastFrameDelta;
% exitTime         = E.Window.LastFlipTime + (E.Window.InterFrameInterval*E.Timing.EyetrackingRiskRate);
timeNow          = GetSecs;

% while timeNow < exitTime && E.Settings.EyetrackingOn
if strcmpi(E.Eyetracker,'EyeLink') && E.Computer.EyeLinkConnected% EL is setup and wanted
    try %to get a EyeLink Sample
        if Eyelink('NewFloatSampleAvailable') > 0
            evt                         = Eyelink('NewestFloatSample');
            sampleCount = sampleCount + 1;
            E.EyePos.X(intLe,intLeTrial,sampleCount,:)        = single(evt.gx);
            E.EyePos.Y(intLe,intLeTrial,sampleCount,:)        = single(evt.gy);
            E.EyePos.PA(intLe,intLeTrial,sampleCount,:)       = single(evt.pa);
            E.EyePos.ResXY(intLe,intLeTrial,sampleCount,:)    = single([evt.rx, evt.ry]);
            E.EyePos.Time(intLe,intLeTrial,sampleCount)       = evt.time;
            status = sprintf('OK');
            E.LastEyePos = [single(evt.gx), single(evt.gy)];
            numETSamps = numETSamps + 1;
            E.EyePos.OverSample(intLe,intLeTrial,frameThisTrial,numETSamps)       = numETSamps; % timestamp is returned
            
        end
    catch  % graceful failure if EyeLink fails for some reason?
        sampleCount = sampleCount + 1;
        E.EyePos.X(intLe,intLeTrial,sampleCount,:)        = single(NaN);
        E.EyePos.Y(intLe,intLeTrial,sampleCount,:)        = single(NaN);
        E.EyePos.PA(intLe,intLeTrial,sampleCount,:)       = single(NaN);
        E.EyePos.ResXY(intLe,intLeTrial,sampleCount,:)    = single(NaN);
        E.EyePos.Time(intLe,intLeTrial,sampleCount)       = timeNow;
        E.LastEyePos = [NaN NaN];
        status = sprintf('No Sample');
    end
elseif strcmpi(E.Eyetracker,'EyeTribe') && E.Computer.EyeTribeConnected% ET is setup and wanted
    sampleCount = sampleCount + 1;
    try % Seems to return 0s if no new position - will need filtering
        [~, xET, yET] = eyetribe_sample(E.EyetribeConnection);
        %format: ets = eyetribe_send_command(E.EyetribeConnection, 'time')
        [success, sizeET] = eyetribe_pupil_size(E.EyetribeConnection);
        E.EyePos.X(intLe,intLeTrial,sampleCount,:)        = single(xET);
        E.EyePos.Y(intLe,intLeTrial,sampleCount,:)        = single(yET);
        E.EyePos.PA(intLe,intLeTrial,sampleCount,:)       = single(sizeET);
        E.EyePos.ResXY(intLe,intLeTrial,sampleCount)      = single(NaN); % don't think this is returned
        E.EyePos.Time(intLe,intLeTrial,sampleCount)       = timeNow; % timestamp is returned
        E.LastEyePos = single([xET, yET]);
        if success && xET ~= 0
            status = 'OK';
            numETSamps = numETSamps + 1;
            E.EyePos.OverSample(intLe,intLeTrial,frameThisTrial,numETSamps)       = numETSamps;
        else
            status = 'No New Sample';
            E.LastEyePos = [NaN NaN];
            
        end
    catch % error in ET communication
        E.EyePos.X(intLe,intLeTrial,sampleCount)           = single(NaN);
        E.EyePos.Y(intLe,intLeTrial,sampleCount)           = single(NaN);
        E.EyePos.PA(intLe,intLeTrial,sampleCount)          = single(NaN);
        E.EyePos.ResXY(intLe,intLeTrial,sampleCount)       = single(NaN);
        E.EyePos.Time(intLe,intLeTrial,sampleCount)        = timeNow;
        status = 'Eyetracker error';
        E.Error = 'Eyetribe communication error';
        E.LastEyePos = single([NaN NaN]);
        
    end
    % Tobii code moved to the top of code to check for timing
    % improvement
    
elseif strcmpi(E.Eyetracker,'SimEyelink')% ET wanted, but not connected = simulate
    sampleCount = sampleCount + 1;
    E.EyePos.X(intLe,intLeTrial,sampleCount,:)        = [0 0];% single(E.Window.PTBScreenWidth*rand([1,2]));
    E.EyePos.Y(intLe,intLeTrial,sampleCount,:)        = [0 0];% single(E.Window.PTBScreenHeight*rand([1,2]));
    E.EyePos.PA(intLe,intLeTrial,sampleCount,:)       = [0 0]; % single(500*rand([1,2]));
    E.EyePos.ResXY(intLe,intLeTrial,sampleCount,:)    = [0 0]; single(25+2*rand([1,2]));
    E.EyePos.Time(intLe,intLeTrial,sampleCount)       = timeNow;
    E.LastEyePos = [squeeze(E.EyePos.X(intLe,intLeTrial,sampleCount,:)),squeeze(E.EyePos.Y(intLe,intLeTrial,sampleCount,:))];
    status = sprintf('OK');
    %         WaitSecs(1/E.Settings.EyetrackingSampleRate); % simulate ?NewFloatAvailable.
    numETSamps = numETSamps + 1;
    E.EyePos.OverSample(intLe,intLeTrial,frameThisTrial,numETSamps)       = numETSamps;
end

E = PsychSandboxGetKeyboardAndMouse(E);

%     if E.RepeatKeyPressed || E.ExitKeyPressed
%         timeNow = exitTime; % no point continuing
if E.Settings.FeedbackLevel > 0
    if E.RepeatKeyPressed
        fprintf(1,'Repeat key pressed on Trial %i-%i\n', intLe,intLeTrial);
        P.RepeatedTrial(intLe,intLeTrial) = 1;
        %Reset things here as in non-time critical part of function
        frameThisTrial = 0;
        sampleCount   = 0;
    elseif E.ExitKeyPressed
        fprintf(1,'Exit key pressed during Trial %i-%i\n', intLe,intLeTrial\n');
    end
end
%     else
%         timeNow = GetSecs;  % Keep calm and carry on
%     end
E.SamplesPerTrial(intLe,intLeTrial) = sampleCount;
% end




