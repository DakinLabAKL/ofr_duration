% OFR_Duration analysis trial by trial base

% Analysis of just recorded data

%convert cells to structures
if iscell(E)
    E = E{1};
    P = P{1};
    S = S{1};
end

% check for droped frames
if 0
    for i = 1:1:length(P.TrialNumbers)
        theInt= P.InterList(i);
        TrialofTheInt = P.TrialNumbers(i);
        InterpolatedFrames   = squeeze(squeeze(E.Window.InterpolatedFrames(theInt,TrialofTheInt,1:round(S.StimulusLength./E.Window.InterFrameInterval))));
        InterpolatedFramesNo = sum(InterpolatedFrames);
        FrameRate = double(1./diff(squeeze(E.Window.FrameFlipTime(theInt, TrialofTheInt, 1:end))));%.*double(~InterpolatedFrames); %  double(InterpolatedFrames);
        figure(90),plot(FrameRate,'ro')
        title(sprintf('Trial: %2.2f Int: %2.2f  TrialofThisInt: %2.2f Interpolated frames: %3.0f'  ,i, theInt, TrialofTheInt, InterpolatedFramesNo ))
        drawnow,pause
    end
end
    ObserverName = 'test';%{E.Folders.SaveFileName(1:9)}; % SMD_VFLF' 'SCD_VFLF'};
    JustFirstSample = 0; %  1 = just use the first sample per each frame (1 is not working with tobii)
    EyeList       = 'LR';
    TheRuns       = {1}; % We have one final run runs of 288 trials for four contrast levelsE.Settings.BaseContrastLevels
    TheConditions = 2; % 2AFC111
    conLoop       = 1;
    MaxSaccSpeedDegPerSec = 500; % fixed - determined by minimisation
    paStdThresh           = 2.75; %  used to find and remove blinks
    FrameMargin           = 12; % number of eye tracking samples to disgard either side of a blink. was 5
    SSpdWS                = 4; % window in milisecond, after and before each point to calculate sliding window speed.
%     Smooth4SpeedCalc =1; % 1= use sgolay filter to smooth eye-position for speed calculation
%     Sgolength = 3;
    IVvalues= [1];
    IVloop=1;
    
    TheEyes = [1, 2]; % The examined eyes; 1=LE 2=RE [1,2]= Binocular
    runLoop = 1;
    if  strcmp( E.Eyetracker,'Tobii')
        X2PixConvertRatio = E.Window.PTBScreenWidth;
        Y2PixConvertRatio = E.Window.PTBScreenHeight;
    else
        X2PixConvertRatio = 1;
        Y2PixConvertRatio = 1;
    end
    % Restructure the collected data
    for subLoop = 1
        for TrialLoop = 1: P.CurrentTrialNo%P.TotalTrials
            ThisTrialIntLe    = P.InterList(TrialLoop);     % the interleaved condition of this trial (1=rightward  2=leftward stimulus movement )
            ThisIntLeRept     = P.TrialNumbers(TrialLoop);  % the number of repetitions of a this interleaved condition by this trial
            ThisTrialMaskInd  = P.MaskInds(TrialLoop);      % the mask index of this trial
            ThisTrialStimInd  = 1;%P.StimInds(TrialLoop);      % the stimulus index of this trial
            ThisTrialContInd  = P.ContrastInds(TrialLoop);  % the contrast index of this trial
            ThisTrialFixInd   = find(E.Settings.IsFixationMarker==P.FixMrkInds(TrialLoop));    % the presence or absence of fixation marker in this trial 0=no fixation marker over the carrier and 1= fixation marker over the carrier

            for eyeLoop = 1:length(TheEyes)
                % Get eye-position data
                xDataIn         = squeeze(E.EyePos.X(ThisTrialIntLe, ThisIntLeRept, :, eyeLoop)).* X2PixConvertRatio;
                yDataInvIn      = squeeze(E.EyePos.Y(ThisTrialIntLe, ThisIntLeRept, :, eyeLoop)).* Y2PixConvertRatio;
                yDataIn         = -(yDataInvIn(:) - E.Window.PTBScreenHeight);% turn the Y direction from screen coordiante to mathematic coordinate. if this line is in the code, make sure that in dirdiff the angles are in correct order which should be [0 pi/2 pi 3pi/2]
                FrameFlipDataIn = squeeze(E.Window.FrameFlipTime(ThisTrialIntLe, ThisIntLeRept, :)); % keep the frame flip time for later processing
                
                paDataIn      = squeeze(E.EyePos.PA(ThisTrialIntLe,  ThisIntLeRept, :, eyeLoop));  % pa: pupil area
                tDataIn       = squeeze(E.EyePos.Time(ThisTrialIntLe, ThisIntLeRept, :));
                
                StimulusDirection = DegToRad(E.Settings.StimulusDirection(ThisTrialIntLe));
                
                SamplingRate =IVvalues(IVloop);
                %500; % 800 deg
                
                OFR_DurationProcessing
                
                if 1%E.plotting % TheIndices(ThisIntLeRept)==259%
                    if   1% SldGain<0.5 && (ThisTrialContInd==4) %(ThisTrialMaskInd==9)&&SldGain<0.3 OKNscore==0 %(subLoop==3)% &(abs(OKNGain) <0.4)& % ismember(ThisTrialMaskInd, [1 5 9 ] ) && ismember(ThisIntLeRept, [ 1 8])  && ismember(ThisTrialIntLe,[1 2])%|| ThisTrialStimInd == 5
                        
                        BelowThresh = find(Speed<Threshold);
                        AboveThresh = find(Speed>=Threshold);
                        PursOrNot = Speed<Threshold;
                        
                        SeqBelowThresh = find((PursSpeed) < Threshold);
                        SeqAboveThresh = find((PursSpeed) >= Threshold);
                        
                        SpeedDb = 20.*log10(Speed);
                        xDallDb = 20.*log10(abs(xDall));
                        yDallDb = 20.*log10(abs(yDall));
                        
                        PursSpeedDb = 20.*log10(PursSpeed);
                        
                        if ~isempty(Speed)
                            maxSpdVal = max(Speed);
                        else
                            maxSpdVal = 1;
                        end
                        StimSpeedPlt = S.SpeedPixPerMS.*Tall;
                        
                        EstOffsetFromHist = Tall.*(SpeedFromHistPixPerSec/1000);
                        subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,((S.SpeedPixPerSec/1e6).*Tall),'k-.', Tall,((SpeedFromHistPixPerSec/1e6).*Tall),'m-.' );
                        % subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall,EstOffsetFromHist,'b-',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimSpeedPlt,'k-.',Tall,(StimSpeedPlt.*HistGain)+E.Window.PTBScreenWidthCenter,'m-.' );
                        %                                     legend('Rotated Eye X Position', 'Saccade', 'Tracking', '10 deg/s', sprintf('Gain %2.2f',HistGain), 'Location', 'Best')
                        ylim([0 E.Window.PTBScreenWidth]),
                        title(sprintf('Subject:%i %sE %iAFC Contrast:%2.2f ; Mask:%i Inter:%i Repetition:%i   XData',subLoop, EyeList(eyeLoop), TheConditions(conLoop), E.Settings.BaseContrastLevels(ThisTrialContInd)*100, ThisTrialMaskInd, ThisTrialIntLe , ThisIntLeRept)); shg
                        subplot(2,4,2);plot( Tall,Yall,'b-',Tall(AboveThresh),Yall(AboveThresh),'r.',Tall(BelowThresh),Yall(BelowThresh),'g.');
                        title('YData');
                        ylim([0 E.Window.PTBScreenHeight]),
                        subplot(2,4,3);plot( xData0,yData0,'b-',xData00, yData00,'r.');
                        title(sprintf('Unprocessed Eye Gaze on Screen (%2.0f deg) ',E.Settings.StimulusDirection(ThisTrialIntLe)));
                        ylim([0 E.Window.PTBScreenHeight]),  xlim([0 E.Window.PTBScreenWidth]),
                        legend('Original Eye Gaze', 'Rotated Eye Gaze')
                        
                        %                                     subplot(2,4,4),plot( Tall,Xall,'b-',Tall(PursStartInd),Xall(PursStartInd),'r*',Tall(PursEndInd),Xall(PursEndInd),'ks');ylim([0 E.Window.PTBScreenWidth]),
                        %                                     legend('eye position', 'Star of Pursuit', 'End of Pursuit')
                        subplot(2,4,5);plot( tDall,xDall,'b-',tDall(AboveThresh),xDall(AboveThresh),'r.',tDall(BelowThresh),xDall(BelowThresh),'g.',tDall,HistGain.*ones(size(tDall)),'K-');
                        title('XSpeed'); legend('Speed','Saccade', 'Tracking',sprintf('Gain=%2.2f',HistGain)),ylim([-maxSpdVal maxSpdVal])
                        subplot(2,4,6);plot(tDall,yDall,'b-',tDall(AboveThresh),yDall(AboveThresh),'r.',tDall(BelowThresh),yDall(BelowThresh),'g.',tDall,OKNGain.*ones(size(tDall)),'K-');
                        ylim([-maxSpdVal maxSpdVal]),title('YSpeed');
                        subplot(2,4,7);plot(tDall,Speed,'b-',tDall(AboveThresh),Speed(AboveThresh),'r.',tDall(BelowThresh),Speed(BelowThresh),'g.',tDall,OKNGain.*ones(size(tDall)),'K-');
                        ylim([-maxSpdVal maxSpdVal]),title('Directional Speed');
                        subplot(2,4,7);polarplot(SpeedAng,SpeedDb,'b.',SpeedAng(AboveThresh),SpeedDb(AboveThresh),'ro',SpeedAng(BelowThresh),SpeedDb(BelowThresh),'go')
                        %                                     subplot(2,4,8),plot(2:length(tData),1./(diff(tData./1000)),'b.-'),
                        %                                     title(sprintf('Mean first sample frequency %2.2f',mean(1./(diff(tData./1000)))))
                        %                                     %                                     subplot(2,4,8);plot(1:length(tData0),tData0,'b.')
                        %                                         polarplot(PursSpeedAng,PursSpeedDb,'b.',PursSpeedAng(SeqAboveThresh),PursSpeedDb(SeqAboveThresh),'ro',PursSpeedAng(SeqBelowThresh),PursSpeedDb(SeqBelowThresh),'go')
                        set (gcf, 'Units', 'normalized', 'Position', [0  0.1  1  0.8]); %  [1.5000    0.0778    0.7500    0.6718]);
                        
                        fprintf('Trial: %i; %sE: OKNscore: %i;  Threshold: %2.2f;  HistGain: %2.2f; OKN Consistency: %2.2f  TotalDisRatio: %2.2f; ConsDistRatio: %2.2f\n',...
                            TrialLoop, EyeList(eyeLoop), OKNscore, Threshold, HistGain, OKNConsistency, TotalDisRatio, ConsistentDistRatio);
                        drawnow
                        
                    end
                end
                
                % saving the result
                AllXstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}                  = Xall;
                AllYstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}                  = Yall;
                AllPstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}                  = Pall;
                AllTstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}                  = Tall;
                AllXSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}               = xDall;
                AllYSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}               = yDall;
                AllTSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}               = tDall;
                AllSpdVecstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}             = Speed;
                AllSpdAngstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}             = SpeedAng;
                AllThreshstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}             = Threshold;
                AllPursuitstore{subLoop, conLoop,eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}             = PursuitOrSaccade;
                AllOKNscorestore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}           = OKNscore;
                AllOKNConsistencystore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}     = double(OKNConsistency);
                AllOKNConsDistRatio{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}        = ConsistentDistRatio;
                AllConsOKNgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}        = ConsistentGainRec;
                AllThetaVariRatiostore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}     = TheThetaVariablilityRatio;
                AllTotDistRatiostore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}       = TotalDisRatio;
                AllKeyRespstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}            = P.Response(TrialLoop);
                AllMeanPupilAreastore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}      = PupilAreaFromHistPix;
                AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}              = HistGain; %SpeedFromHistDegPerSec/S.SpeedDegPerSec;
                
            end
        end
    end
    
%     %% ploting the data 
%     subLoop =1;
%     conLoop=1;
%     eyeLoop = [1,2];
%     runLoop =1;
%     ThisTrialIntLe = 1: P.NoInterleaved;
%     ThisTrialStimInd = 1: numel(P.PossStimValues{1});
%     ThisTrialContInd = 1: numel(P.PossContrastValues{1});
%     ThisTrialMaskInd  = 1: numel(P.PossMaskValues{1});
%     ThisIntLeRept = 1:E.Settings.LocalTrialNo;
%     Xdata = P.PossContrastValues{1}*100;
%     Cmap = [0.4 0.4 0.4];
%     for eyeLoop = 1:numel(EyeList)
%     OKN_Gain = squeeze([AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}]);
%     fprintf('%sE Gain : %2.2f � %2.2f\n',EyeList(eyeLoop), nanmean(OKN_Gain),nanstd(OKN_Gain))
%     figure(10),
%     subplot(1,2,eyeLoop);
%     errorbar(Xdata,mean(OKN_Gain),std(OKN_Gain),'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap,'Color',Cmap);
%     hold on;
%     plot(Xdata,OKN_Gain,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap-0.2,'Color',Cmap-0.2);
%  if eyeLoop ==1
%     ylabel('OKN-Gain')
% xlabel('Contrast (%)')
% legend('Mean � SD','Gain of each trial','location','best')
%  end
%     title(sprintf('%sE',EyeList(eyeLoop)))
%     axis([95 105 0.2 1.1])
%     end 
   
% eyeLoop = [1,2];
% OKN_Gain2 = cell2mat(reshape(squeeze({AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisIntLeRept, ThisTrialFixInd}}),2,2,8));


    
   