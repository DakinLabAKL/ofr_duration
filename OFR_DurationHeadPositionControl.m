% % Check for head tilt and rotation
% if strcmpi(E.Eyetracker,'Tobii')
%     E.GoodHeadPositionFlag = [];
%     %Get gaze origin points
%     if isempty(TobiiSample.left_gaze_origin_in_user_coordinate_system)&&isempty(TobiiSample.right_gaze_origin_in_user_coordinate_system)
%         LE_origin = E.EyePos.LE_Gazeorigin(intLe,intLeTrial,sampleCount,:); %X, Y, and Z in user coordinate system
%         RE_origin = E.EyePos.RE_Gazeorigin(intLe,intLeTrial,sampleCount,:);
%     elseif isempty(TobiiSample.left_gaze_origin_in_user_coordinate_system)
%         LE_origin = E.EyePos.LE_Gazeorigin(intLe,intLeTrial,sampleCount,:);
%         RE_origin = TobiiSample.right_gaze_origin_in_user_coordinate_system(end,:);
%     elseif isempty(TobiiSample.right_gaze_origin_in_user_coordinate_system)
%         LE_origin = TobiiSample.left_gaze_origin_in_user_coordinate_system(end,:);
%         RE_origin = E.EyePos.RE_Gazeorigin(intLe,intLeTrial,sampleCount,:);
%     else
%         LE_origin = TobiiSample.left_gaze_origin_in_user_coordinate_system(end,:);
%         RE_origin = TobiiSample.right_gaze_origin_in_user_coordinate_system(end,:);
%     end
%     E.EyePos.LE_Gazeorigin(intLe,intLeTrial,sampleCount,:) = double(LE_origin);
%     E.EyePos.RE_Gazeorigin(intLe,intLeTrial,sampleCount,:) = double(RE_origin);
%     
%     X_EyeSeparation_UCS = abs(LE_origin(1)-RE_origin(1)); %ucs user coordinate system x= rightward more positive
%     Y_EyeSeparation_UCS = abs(LE_origin(2)-RE_origin(2)); % Y : upward more positive
%     Z_EyeSeparation_UCS = abs(LE_origin(3)-RE_origin(3)); % Z : far from center of Tobii perpendicular to X and Y
%     
%     TiltAngle_aroundZ_UCS = atand((0.5*Y_EyeSeparation_UCS)/X_EyeSeparation_UCS);
%     TiltAngle_aroundY_UCS = atand((0.5*Z_EyeSeparation_UCS)/X_EyeSeparation_UCS);
%     if (TiltAngle_aroundZ_UCS<5) && (TiltAngle_aroundY_UCS<5)
%         E.GoodHeadPositionFlag = true;
%         E.HeadPositionFlagCol  = [0 E.Window.White/2 0]; % green code
%     else
%         E.GoodHeadPositionFlag = false;
%         E.HeadPositionFlagCol  = [E.Window.White/2 0  0];  % red code
%     end
%     E.GoodHeadPositionFlags(intLe,intLeTrial,sampleCount) = E.GoodHeadPositionFlag;
%     
% elseif strcmpi(E.Eyetracker,'SimEyelink')
%     E.GoodHeadPositionFlag = true;
%     E.HeadPositionFlagCol  = [0.5 0.5  0.5];
% end
    E.GoodHeadPositionFlag = true; % hardcoded to test other parts of the code
    E.HeadPositionFlagCol  = [0.5 0.5 0.5];